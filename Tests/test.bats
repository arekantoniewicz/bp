#!/usr/bin/env bats

@test "Diff" {
  result="$(../TDiff/bin/tdiff 123 12 -f adcharacter)"
  [ "$result" = "*** 123 2020-12-22 13:40:50 +0100
--- 12 2020-12-22 13:40:50 +0100
@@ 1,(.),1
***************
*** 1,4 ****
--- 1,3 ----
t  1
t  2
t- 3" ]
}

@test "Diff3 one file addition on end" {
  result="$(../TDiff/bin/tdiff3 12 12 123 -f adcharacter)"
  [ "$result" = "123" ]
}

@test "Diff3 one file deletion on end" {
  result="$(../TDiff/bin/tdiff3 123 123 12 -f adcharacter)"
  [ "$result" = "12" ]
}

@test "Diff3 two file addition on end" {
  result="$(../TDiff/bin/tdiff3 123 12 123 -f adcharacter)"
  [ "$result" = "123" ]
}

@test "Diff3 two file deletion on end" {
  result="$(../TDiff/bin/tdiff3 12 123 12 -f adcharacter)"
  [ "$result" = "12" ]
}


@test "Diff3 one file addition on start" {
  result="$(../TDiff/bin/tdiff3 23 23 123 -f adcharacter)"
  [ "$result" = "123" ]
}

@test "Diff3 one file deletion on start" {
  result="$(../TDiff/bin/tdiff3 123 123 23 -f adcharacter)"
  [ "$result" = "23" ]
}

@test "Diff3 two file addition on start" {
  result="$(../TDiff/bin/tdiff3 123 23 123 -f adcharacter)"
  [ "$result" = "123" ]
}

@test "Diff3 two file deletion on start" {
  result="$(../TDiff/bin/tdiff3 23 123 23 -f adcharacter)"
  [ "$result" = "23" ]
}

@test "Diff3 one file substitution on end" {
  result="$(../TDiff/bin/tdiff3 123 123 124 -f adcharacter)"
  [ "$result" = "124" ]
}

@test "Diff3 two file substitution on end" {
  result="$(../TDiff/bin/tdiff3 124 123 124 -f adcharacter)"
  [ "$result" = "124" ]
}

@test "Diff3 one file substitution on start" {
  result="$(../TDiff/bin/tdiff3 123 123 423 -f adcharacter)"
  [ "$result" = "423" ]
}
@test "Diff3 two file substitution on start" {
  result="$(../TDiff/bin/tdiff3 423 123 423 -f adcharacter)"
  [ "$result" = "423" ]
}



@test "Diff3 conflict1" {
  result="$(../TDiff/bin/tdiff3 124 1234 1254 -f adcharacter)"
  [ "$result" = "12
<<<<<<< 124
||||||| 1234
3
=======
5
>>>>>>> 1254
4" ]
}

@test "Diff3 conflict2" {
  result="$(../TDiff/bin/tdiff3 124 123 12 -f adcharacter)"
  [ "$result" = "12
<<<<<<< 124
4
||||||| 123
3
=======
>>>>>>> 12" ]
}


@test "Diff3 conflict3" {
  result="$(../TDiff/bin/tdiff3 124 123 153 -f adcharacter)"
  [ "$result" = "1
<<<<<<< 124
2
4
||||||| 123
2
3
=======
5
3
>>>>>>> 153" ]
}

@test "Diff3 conflict4" {
  result="$(../TDiff/bin/tdiff3 124 123 153 -f adcharacter)"
  [ "$result" = "1
<<<<<<< 124
2
4
||||||| 123
2
3
=======
5
3
>>>>>>> 153" ]
}

@test "Simple csv files" {
  result="$(../TDiff/bin/tdiff simple1.csv simple2.csv -f adsimplecsvdefinition -C 0)"
  [ "$result" = "*** simple1.csv 2021-01-04 11:49:56 +0100
--- simple2.csv 2021-01-04 11:49:57 +0100
@@ header,.*\r\n,line
@@ line,([^\r\n;]*),separator
@@ separator,(?:\r\n|;),line
***************
*** 6,10 ****
--- 5,5 ----
w- \r\n
t- grey07
w- ;
w- ;
t- 2070
w- ;
t- Laura
w- ;
t- Grey
***************
*** 15,15 ****
--- 10,10 ----
w  ;
t- Johnson
t+ Johanson
***************
*** 18,18 ****
--- 13,13 ----
w  ;
t- 9346
t+ 9319" ]
}
