#include <string>
#include <iostream>
#include "InputOutputDiff3.h"
#include "getopt.h"
#include "Utils.h"
#include <fstream>
#include "Tokenizer.h"
#include "Differentiate.h"

int main(int argc, char *argv[])
{
	try
	{
		std::vector<std::string> vs;

		while (true)
		{
			int c;
			static struct option long_options[] =
				{
					{"automata", required_argument, 0, 'a'},
					{"help", no_argument, 0, 'h'},
					{"file-automaton", required_argument, 0, 'f'}};
			int option_index = 0;
			c = getopt_long(argc, argv, "ha:f:", long_options, &option_index);
			if (c == -1)
			{
				break;
			}
			switch (c)
			{
			case 'f':
			{
				std::ifstream f4(optarg);
				std::string l1;
				while (std::getline(f4, l1))
				{
					vs.push_back(l1);
				}
				break;
			}
			case 'a':
				vs = utils::parse_one_line_automata(optarg);
				break;
			case 'h':
				input_output_diff3::printhelp();
				return 0;
			}
		}

		std::string input_file1, input_file2, input_file3;

		if (vs.empty())
		{
			throw std::logic_error("Automaton not defined");
		}

		auto a = argv[optind];
		if (a == nullptr)
		{
			throw std::logic_error("Need 3 files to specify");
		}

		input_file1 = a;
		a = argv[++optind];
		if (a == nullptr)
		{
			throw std::logic_error("Need 3 files to specify");
		}
		input_file2 = a;
		a = argv[++optind];
		if (a == nullptr)
		{
			throw std::logic_error("Need 3 files to specify");
		}
		input_file3 = a;

		input_output_diff3::print_three_files(input_file1, input_file2, input_file3, vs);
	}
	catch (std::exception &e)
	{
		std::cout << "ERROR:" << e.what() << std::endl;
		return 1;
	}
	return 0;
}
