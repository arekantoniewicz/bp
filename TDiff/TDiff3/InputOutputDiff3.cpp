#include <string>
#include "../TMergeToolsLibrary/Utils.h"
#include "../TMergeToolsLibrary/Differentiate.h"
#include "InputOutputDiff3.h"

namespace input_output_diff3
{
	std::string inline string_from_s_token(tokenizer::s_token &s)
	{
		return std::string(s.text.begin, s.text.length);
	}
	//check wheter in reality there is no conflict but its same change on both files
	//return true if conflict is not really conflict
	bool inline check_conflict_and_print(conflict &c, differentiate::file_differences &fd_mine, differentiate::file_differences &fd_your)
	{
		auto delta_diff_mine = c.end_m_diff - c.start_m_diff;
		auto delta_diff_your = c.end_y_diff - c.start_y_diff;
		if (delta_diff_mine != delta_diff_your)
		{
			return false;
		}
		for (size_t i = 0; i <= delta_diff_mine; i++)
		{
			auto mine_diff = fd_mine.diffs[i + c.start_m_diff];
			auto your_diff = fd_your.diffs[i + c.start_y_diff];
			auto mine_source_token = fd_mine.tokens_source.s_tokens[mine_diff.token_index_source];
			auto mine_source_string = string_from_s_token(mine_source_token);
			auto mine_target_token = fd_mine.tokens_target.s_tokens[mine_diff.token_index_target];
			auto mine_target_string = string_from_s_token(mine_target_token);

			auto your_source_token = fd_your.tokens_source.s_tokens[your_diff.token_index_source];
			auto your_source_string = string_from_s_token(your_source_token);
			auto your_target_token = fd_your.tokens_target.s_tokens[your_diff.token_index_target];
			auto your_target_string = string_from_s_token(your_target_token);

			if (mine_diff.type != your_diff.type)
			{
				return false;
			}
			if (mine_diff.type == differentiate::insertion)
			{
				if (mine_target_string != your_target_string)
				{
					return false;
				}
			}
			if (mine_diff.type == differentiate::deletion)
			{
				if (mine_source_string != your_source_string)
				{
					return false;
				}
			}
			if (mine_diff.type == differentiate::substitution)
			{
				if (mine_source_string != your_source_string || mine_target_string != your_target_string)
				{
					return false;
				}
			}
		}

		for (size_t i = c.start_m_diff; i <= c.end_m_diff; i++)
		{
			auto diff = fd_mine.diffs.at(i);
			if (diff.type != differentiate::deletion)
			{
				std::cout.write(diff.target.text.begin, diff.target.text.length);
				std::cout.flush();
			}
		}

		return true;
	}

	void inline print_conflict(conflict &c, differentiate::file_differences &fd_mine, differentiate::file_differences &fd_your)
	{
		if (check_conflict_and_print(c, fd_mine, fd_your))
		{
			return;
		}
		std::cout << std::endl;
		std::cout << "<<<<<<< " << fd_mine.file_path_target.c_str() << std::endl;
		for (size_t i = c.start_m_diff; i <= c.end_m_diff; i++)
		{
			auto diff = fd_mine.diffs.at(i);
			if (diff.type != differentiate::deletion)
			{
				utils::replace_to_ws_and_write(diff.target, std::cout);
				std::cout << std::endl;
			}
		}
		std::cout << "||||||| " << fd_mine.file_path_source.c_str() << std::endl;
		for (size_t i = c.start_old_token; i <= c.end_old_token; i++)
		{
			auto line = fd_mine.tokens_source.s_tokens[i];
			utils::replace_to_ws_and_write(line, std::cout);
			std::cout << std::endl;
		}

		std::cout << "=======" << std::endl;
		for (size_t i = c.start_y_diff; i <= c.end_y_diff; i++)
		{
			auto diff = fd_your.diffs.at(i);
			if (diff.type != differentiate::deletion)
			{
				utils::replace_to_ws_and_write(diff.target, std::cout);
				std::cout << std::endl;
			}
		}
		std::cout << ">>>>>>> " << fd_your.file_path_target.c_str() << std::endl;
	}


	std::vector<conflict> find_conflicts(std::vector<utils::diffs_group> &mine, std::vector<utils::diffs_group> &your, differentiate::file_differences &mine_fd, differentiate::file_differences &your_fd)
	{
		std::vector<conflict> result;
		for (size_t i = 0; i < mine.size(); i++)
		{
			for (size_t j = 0; j < your.size(); j++)
			{
				auto m = mine.at(i);
				if (m.diffs.front().type == differentiate::insertion)
				{
					//m.diffs.front().token_index_source++;
				}
				size_t mine_start = m.diffs.front().token_index_source - 1;
				size_t mine_ends = m.diffs.back().token_index_source + 1;

				auto y = your.at(j);
				if (y.diffs.front().type == differentiate::insertion)
				{
					//y.diffs.front().token_index_source++;
				}
				size_t your_start = y.diffs.front().token_index_source - 1;
				size_t your_ends = y.diffs.back().token_index_source + 1;
				if ((mine_start > your_start && mine_start < your_ends) || (your_start > mine_start && your_start < mine_ends) || your_start == mine_start)
				{
					if (m.conflict)
					{
						your.at(j).conflict = true;
						your.at(j).conflict_id = m.conflict_id;
						result.at(y.conflict_id).end_y_diff = y.start_diff_index + y.diffs.size() - 1;
						result.at(y.conflict_id).end_old_token = std::max(result.at(y.conflict_id).end_old_token, y.diffs.back().token_index_source);
						size_t index_of_last_diff_with_source_token = 0;
						while (mine_fd.diffs.at(index_of_last_diff_with_source_token).token_index_source <= result.at(y.conflict_id).end_old_token)
						{
							index_of_last_diff_with_source_token++;
						}
						result.at(y.conflict_id).end_m_diff = std::max(result.at(y.conflict_id).end_m_diff, index_of_last_diff_with_source_token - 1);
					}
					else if (y.conflict)
					{
						mine.at(i).conflict = true;
						mine.at(j).conflict_id = y.conflict_id;
						result.at(y.conflict_id).end_m_diff = m.start_diff_index + m.diffs.size() - 1;
						result.at(y.conflict_id).end_old_token = std::max(result.at(y.conflict_id).end_old_token, m.diffs.back().token_index_source);

						size_t index_of_last_diff_with_source_token = 0;
						while (your_fd.diffs.at(index_of_last_diff_with_source_token).token_index_source <= result.at(y.conflict_id).end_old_token)
						{
							index_of_last_diff_with_source_token++;
						}
						result.at(y.conflict_id).end_y_diff = std::max(result.at(y.conflict_id).end_y_diff, index_of_last_diff_with_source_token - 1);
					}
					else
					{
						conflict c{};
						if (m.diffs.front().token_index_source < y.diffs.front().token_index_source)
						{
							c.start_old_token = m.diffs.front().token_index_source;
							if (m.diffs.front().type == differentiate::insertion)
							{
								c.start_old_token++;
							}
						}
						else
						{
							c.start_old_token = y.diffs.front().token_index_source;
							if (y.diffs.front().type == differentiate::insertion)
							{
								c.start_old_token++;
							}
						}

						//c.start_old_token =
						//	m.diffs.front().token_index_source < y.diffs.front().token_index_source
						//		? m.diffs.front().token_index_source
						//		: y.diffs.front().token_index_source;
						size_t index = 0;
						while (your_fd.diffs.at(index).token_index_source < c.start_old_token)
						{
							index++;
						}
						c.start_y_diff = std::min(index, y.start_diff_index);

						index = 0;
						while (mine_fd.diffs.at(index).token_index_source < c.start_old_token)
						{
							index++;
						}
						c.start_m_diff = std::min(index, m.start_diff_index);

						c.end_old_token =
							m.diffs.back().token_index_source < y.diffs.back().token_index_source
								? y.diffs.back().token_index_source
								: m.diffs.back().token_index_source;
						index = 0;
						while (index < your_fd.diffs.size() && your_fd.diffs.at(index).token_index_source <= c.end_old_token)
						{
							index++;
						}
						c.end_y_diff = std::max(y.start_diff_index + y.diffs.size() - 1, index - 1);

						index = 0;
						while (index < mine_fd.diffs.size() && mine_fd.diffs.at(index).token_index_source <= c.end_old_token)
						{
							index++;
						}
						c.end_m_diff = std::max(m.start_diff_index + m.diffs.size() - 1, index - 1);

						result.push_back(c);
						your.at(j).conflict = true;
						your.at(j).conflict_id = result.size() - 1;
						mine.at(i).conflict = true;
						mine.at(i).conflict_id = result.size() - 1;
					}
				}
			}
		}
		return result;
	}

	//prints from original file up to where group should be applied, then print group content,
	// set up position to where group ends in original file
	void inline print_group(utils::diffs_group &dg, size_t &pos, differentiate::file_differences &fd)
	{
		auto char_ptr = fd.original_text_source.begin + pos;
		auto last_source_index_start = fd.diffs.at(dg.start_diff_index - 1).token_index_source;
		auto token = fd.tokens_source.s_tokens.at(last_source_index_start);
		auto length = token.text.begin - fd.original_text_source.begin - pos + token.text.length;
		std::cout.write(char_ptr, length);
		std::cout.flush();

		auto source_index_start = fd.diffs.at(dg.start_diff_index).token_index_source;
		token = fd.tokens_source.s_tokens.at(source_index_start);
		pos = token.text.begin - fd.original_text_source.begin + token.text.length;
		int counter = -1;
		for (auto &element : dg.diffs)
		{
			counter++;
			std::string to_print;
			switch (element.type)
			{
			case differentiate::insertion:
				std::cout.write(element.target.text.begin, element.target.text.length);
				std::cout.flush();
				break;
			case differentiate::deletion:
			{
				if (counter != 0)
				{
					pos += element.source.text.length;
				}
				break;
			}
			case differentiate::substitution:

				std::cout.write(element.target.text.begin, element.target.text.length);
				std::cout.flush();
				if (counter != 0)
				{
					pos += element.source.text.length;
				}
				break;
			case differentiate::nothing:
				break;
			default:;
			}
		}
	}

	void print_three_files(const std::string &path_mine, const std::string &path_old, const std::string &path_yours, const std::vector<std::string> &automata)
	{
		differentiate::file_differences mine_diffs = utils::compare_files(path_old, path_mine, automata);
		auto your_diffs = utils::compare_files(path_old, path_yours, automata);
		if (!mine_diffs.tokens_source.is_everything_in_tokens() || !mine_diffs.tokens_target.is_everything_in_tokens() || !your_diffs.tokens_target.is_everything_in_tokens())
		{
			std::cout << "For diff 3 whole text needs to be captured (no whitespaces allowed)" << std::endl;
			return;
		}

		std::vector<utils::diffs_group> mine_groupped = utils::group_diffs(mine_diffs.diffs);
		auto your_groupped = utils::group_diffs(your_diffs.diffs);

		auto conflicts = find_conflicts(mine_groupped, your_groupped, mine_diffs, your_diffs);

		size_t pos_in_text = 0;
		size_t index_your = 0;
		size_t index_mine = 0;

		while (index_mine < mine_groupped.size() && index_your < your_groupped.size())
		{
			auto mine = mine_groupped.at(index_mine);
			auto your = your_groupped.at(index_your);
			if (mine.conflict && your.conflict)
			{
				if (mine.conflict_id != your.conflict_id)
				{
					throw std::logic_error("Internal error: conflict doesn't match");
				}
				auto start = mine_diffs.original_text_source.begin + pos_in_text;
				auto length = mine_diffs.tokens_source.s_tokens.at(conflicts.at(mine.conflict_id).start_old_token).text.begin - mine_diffs.original_text_source.begin - pos_in_text;

				std::cout.write(start, length);
				//std::cout << mine_diffs.original_text_source.substr(pos_in_text, mine_diffs.tokens_source.tokens.at(conflicts.at(mine.conflict_id).start_old_token).position_start - pos_in_text);
				pos_in_text = mine_diffs.tokens_source.s_tokens.at(conflicts.at(mine.conflict_id).end_old_token).text.begin - mine_diffs.original_text_source.begin + mine_diffs.tokens_source.s_tokens.at(conflicts.at(mine.conflict_id).end_old_token).text.length;
				auto confl_id = mine.conflict_id;
				print_conflict(conflicts.at(mine.conflict_id), mine_diffs, your_diffs);
				while (index_mine < mine_groupped.size() && mine_groupped.at(index_mine).conflict_id == confl_id)
				{
					index_mine++;
				}
				while (index_your < your_groupped.size() && your_groupped.at(index_your).conflict_id == confl_id)
				{
					index_your++;
				}
				continue;
			}
			if (mine.conflict)
			{
				print_group(your, pos_in_text, your_diffs);
				index_your++;
				continue;
			}
			if (your.conflict)
			{
				print_group(mine, pos_in_text, mine_diffs);
				index_mine++;
				continue;
			}

			if (mine.diffs.front().token_index_source < your.diffs.front().token_index_source)
			{
				print_group(mine, pos_in_text, mine_diffs);
				index_mine++;
			}
			else
			{
				print_group(your, pos_in_text, your_diffs);
				index_your++;
			}
		}
		while (index_mine < mine_groupped.size())
		{
			print_group(mine_groupped.at(index_mine), pos_in_text, mine_diffs);
			index_mine++;
		}
		while (index_your < your_groupped.size())
		{
			print_group(your_groupped.at(index_your), pos_in_text, your_diffs);
			index_your++;
		}
		if (pos_in_text < mine_diffs.original_text_source.length)
		{
			auto start = mine_diffs.original_text_source.begin + pos_in_text;
			auto length = mine_diffs.original_text_source.length - pos_in_text;
			//std::cout << mine_diffs.original_text_source.substr(pos_in_text);
			std::cout.write(start, length);
			std::cout.flush();
		}
	}
} // namespace input_output_diff3
