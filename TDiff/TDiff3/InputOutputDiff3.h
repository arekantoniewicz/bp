#include <string>
#include <utility>
#include <vector>
#include "Differentiate.h"
#include <iostream>
#include "Utils.h"
#include <filesystem>

namespace input_output_diff3
{
	class conflict
	{
	public:
		size_t start_old_token;
		size_t end_old_token;
		size_t start_m_diff;
		size_t end_m_diff;
		size_t start_y_diff;
		size_t end_y_diff;
	};
	void print_three_files(const std::string &path_mine, const std::string &path_old, const std::string &path_yours, const std::vector<std::string> &automata);
	inline void printhelp()
	{
		std::cout << "Arguments mine_file base_file your_file" << std::endl
				  << std::endl;
		std::cout << "-a -automata edges definition delimited by ;" << std::endl;
		std::cout << "Automata definition number_identifier_from,regex_defining_edge,number_identifier_to" << std::endl;
		std::cout << "Using capture groups to capture tokens" << std::endl;
		std::cout << "Alternative automaton definition -f PATH rules delimited by newlines" << std::endl
				  << std::endl;
		std::cout << "Example 1,(\\S*\\s*),1" << std::endl
				  << std::endl;
		std::cout << "Returns merged file with highlighted conflicts" << std::endl;
	}
} // namespace input_output_diff3
