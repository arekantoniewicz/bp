#include <string>
#include <utility>
#include <vector>
#include "Differentiate.h"
#include <iostream>
#include "Utils.h"
#include <filesystem>

namespace input_output_diff
{
	using namespace differentiate;
	namespace fs = std::filesystem;

	//returns paths to all recursive files that are in both folders
	inline std::vector<fs::path> differentiate_folders(const fs::path &source_folder, const fs::path &target_folder)
	{
		std::vector<fs::path> source_files;
		std::vector<fs::path> target_files;
		std::vector<fs::path> result;

		for (auto &p : fs::recursive_directory_iterator(source_folder))
			if (fs::is_regular_file(p))
				source_files.push_back(fs::relative(p.path(), source_folder));

		for (auto &p : fs::recursive_directory_iterator(target_folder))
			if (fs::is_regular_file(p))
				target_files.push_back(fs::relative(p.path(), target_folder));

		std::sort(source_files.begin(), source_files.end());
		std::sort(target_files.begin(), target_files.end());

		auto source_it = source_files.begin();
		auto target_it = target_files.begin();
		while (source_it != source_files.end() && target_it != target_files.end())
		{

			const auto compare = source_it->compare(*target_it);

			if (compare < 0)
			{
				++source_it;
			}
			else if (compare > 0)
			{
				++target_it;
			}
			if (compare == 0)
			{
				result.push_back(*source_it);

				++target_it;
				++source_it;
			}
		}
		return result;
	}

	enum line_to_print_from
	{
		source,
		target,
		both
	};
	enum line_to_print_type
	{
		token,
		whitespace
	};

	class line_to_print
	{
	public:
		line_to_print(tokenizer::sub_text s, line_to_print_from f, line_to_print_type t) : from(f), type(t),str(s.begin,s.length)
		{
		}
		
		line_to_print_from from;
		line_to_print_type type;
		std::string str;
		void print()
		{
			if (str.empty())
			{
				return;
			}
			switch (type)
			{
			case token:
				std::cout << "t";
				break;
			case whitespace:
				std::cout << "w";
				break;
			default:;
			}
			switch (from)
			{
			case source:
				std::cout << "- ";
				break;
			case target:
				std::cout << "+ ";
				break;
			case both:
				std::cout << "  ";
				break;
			default:;
			}
			utils::replace_to_ws(str);
			std::cout << str << std::endl;
		}
	};

	//prints differences with set context size
	void print_file_diffs_context_format(const file_differences &fd, const size_t &context_size);

	//gets paths of two files and automata.Print differences
	inline void compare_and_print(const fs::path &path1, const fs::path &path2, std::vector<std::string> &automata,
								  const size_t &context_size)
	{
		auto a = utils::compare_files(path1, path2, automata);
		input_output_diff::print_file_diffs_context_format(a, context_size);
	}

	void compare_folders(std::string &path1, std::string &path2, std::vector<std::string> &automata,
						 const size_t &context_size);

	//returns true if file, false if directory, exception if neither
	inline bool is_file(const std::string &path)
	{
		if (fs::is_directory(path))
		{
			return false;
		}
		if (fs::is_regular_file(path))
		{
			return true;
		}
		throw std::runtime_error("Nor file or directory");
	}

	inline void printhelp()
	{
		std::cout << "Arguments:" << std::endl
				  << std::endl;
		std::cout << "source_file target_file or source_directory target_directory" << std::endl
				  << std::endl;
		std::cout << "-C -context number_of_lines_using_context" << std::endl;
		std::cout << "-a -automata edges definition delimited by ;" << std::endl;
		std::cout << "Automata definition: number_identifier_starting_state,regex_defining_edge,number_identifier_ending_state" << std::endl;
		std::cout << "Using capture groups to capture tokens" << std::endl;
		std::cout << "Alternative automaton definition -f PATH rules delimited by newlines" << std::endl
				  << std::endl;
		std::cout << "Example 1,(\\S*\\s*),1" << std::endl
				  << std::endl;
		std::cout << "Output context type diff" << std::endl;
	}

	void print_tokens(std::string path, std::vector<std::string> automata_definition);
} // namespace input_output_diff
