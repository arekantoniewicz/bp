#include <string>
#include <vector>
#include <fstream>
#include "Differentiate.h"
#include <iostream>
#include "InputOutputDiff.h"
#include <filesystem>
#include "Utils.h"

namespace input_output_diff
{
	using namespace differentiate;
	using namespace tokenizer;

	void inline insert_lines(std::vector<line_to_print> &lines, tokenizer::sub_text source, tokenizer::sub_text target, line_to_print_type type)
	{
		if (source.length == 0)
		{
			lines.emplace_back(target, line_to_print_from::target, type);
			return;
		}
		if (target.length == 0)
		{
			lines.emplace_back(source, line_to_print_from::source, type);
			return;
		}
		if (utils::subtext_equals(source, target))
		{
			lines.emplace_back(source, line_to_print_from::both, type);
			return;
		}
		else
		{
			lines.emplace_back(source, line_to_print_from::source, type);
			lines.emplace_back(target, line_to_print_from::target, type);
		}
	}
	//Prints one hunk
	void inline print_hunk(const size_t &start_index, const size_t &end_index, const file_differences &fd)
	{
		std::cout << "***************" << std::endl;
		std::cout << "*** " << fd.diffs.at(start_index).token_index_source << "," << fd.diffs.at(end_index).token_index_source << " ****" << std::endl;
		std::cout << "--- " << fd.diffs.at(start_index).token_index_target << "," << fd.diffs.at(end_index).token_index_target << " ----" << std::endl;
		std::vector<line_to_print> lines;
		for (size_t j = start_index; j <= end_index; j++)
		{

			auto diff = fd.diffs.at(j);
			if (diff.type == deletion && diff.token_index_target == 0)
			{
				tokenizer::s_token actual_token_source = fd.tokens_source.s_tokens.at(diff.token_index_source);
				insert_lines(lines, actual_token_source.text, {nullptr, 0}, token);
				continue;
			}
			tokenizer::s_token last_token_target = fd.tokens_target.s_tokens.at(diff.token_index_target - 1);
			tokenizer::s_token actual_token_target = fd.tokens_target.s_tokens.at(diff.token_index_target);
			tokenizer::s_token actual_token_source = fd.tokens_source.s_tokens.at(diff.token_index_source);
			tokenizer::sub_text target_ws = {last_token_target.text.begin + last_token_target.text.length,
											 (size_t)(actual_token_target.text.begin - last_token_target.text.begin) - last_token_target.text.length};
			switch (diff.type)
			{
			case insertion:
			{
				insert_lines(lines, {nullptr, 0}, target_ws, whitespace);
				insert_lines(lines, {nullptr, 0}, actual_token_target.text, token);
				break;
			}
			case deletion:
			{
				tokenizer::s_token last_token_source = fd.tokens_source.s_tokens.at(diff.token_index_source - 1);
				tokenizer::sub_text source_ws = {last_token_source.text.begin + last_token_source.text.length,
												 (size_t)(actual_token_source.text.begin - last_token_source.text.begin) - last_token_source.text.length};

				insert_lines(lines, source_ws, {nullptr, 0}, whitespace);
				insert_lines(lines, actual_token_source.text, {nullptr, 0}, token);
				break;
			}
			case substitution:
			case nothing:
			{
				tokenizer::s_token last_token_source = fd.tokens_source.s_tokens.at(diff.token_index_source - 1);
				tokenizer::sub_text source_ws = {last_token_source.text.begin + last_token_source.text.length,
												 (size_t)(actual_token_source.text.begin - last_token_source.text.begin) - last_token_source.text.length};

				insert_lines(lines, source_ws, target_ws, whitespace);
				insert_lines(lines, actual_token_source.text, actual_token_target.text, token);
				break;
			}
			default:;
			}
		}
		for (auto &element : lines)
		{
			element.print();
		}
	}

	void print_file_diffs_context_format(const file_differences &fd, const size_t &context_size)
	{

		std::cout << "*** " << fd.file_path_source.string() << " " << utils::get_time_modification(fd.file_path_source.string()) << std::endl;

		std::cout << "--- " << fd.file_path_target.string() << " " << utils::get_time_modification(fd.file_path_target.string()) << std::endl;
		for (auto &definition : fd.automata_definition)
		{
			std::cout << "@@ " << definition << std::endl;
		}

		bool in_hunk = false;
		size_t start_of_hunk;
		size_t last_change_in_hunk = 0;
		for (size_t i = 0; i < fd.diffs.size(); i++)
		{
			const auto diff = fd.diffs.at(i);
			if (in_hunk)
			{
				if (diff.type != nothing)
				{
					last_change_in_hunk = i;
				}
				else
				{
					if (last_change_in_hunk + context_size < i)
					{
						in_hunk = false;
						print_hunk(start_of_hunk, i - 1, fd);
					}
				}
			}
			else
			{
				if (diff.type != nothing)
				{
					in_hunk = true;
					start_of_hunk = i >= context_size + 1 ? i - context_size : 1;
					last_change_in_hunk = i;
				}
			}
		}
		if (in_hunk)
		{
			print_hunk(start_of_hunk, fd.diffs.size() - 1, fd);
		}
	}

	void compare_folders(std::string &path1, std::string &path2, std::vector<std::string> &automata,
						 const size_t &context_size)
	{
		auto paths = differentiate_folders(path1, path2);
		for (auto &path : paths)
		{
			compare_and_print(operator/(path1, path), operator/(path2, path), automata, context_size);
		}
	}
	void print_tokens(std::string path, std::vector<std::string> automata_definition)
	{
		auto at = tokenizer::build_automata(automata_definition);

		const char *file1_start;
		auto size_file1 = utils::mmap_file(&file1_start, path.c_str());
		parsed_text pt;
		tokenizer::parse_text(file1_start, size_file1, at, pt);
		for (size_t i = 1; i < pt.s_tokens.size() - 1; i++)
		{
			auto max_number_of_space = 0;
			auto s = pt.s_tokens.size() - 1;
			while (s > 0)
			{
				max_number_of_space++;
				s = s / 10;
			}
			auto current_i_length = 0;
			s = i;
			while (s > 0)
			{
				current_i_length++;
				s = s / 10;
			}
			int number_of_spaces = max_number_of_space - current_i_length;

			auto token = pt.s_tokens.at(i).text;
			std::cout << "t" << i;
			for (int i = 0; i <= number_of_spaces; i++)
			{
				std::cout << " ";
			}

			std::cout.write(token.begin, token.length);
			std::cout << std::endl; //this does flush as well
		}
	}
} // namespace input_output_diff
