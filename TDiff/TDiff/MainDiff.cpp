#include <sstream>
#include <fstream>
#include "Tokenizer.h"
#include "Differentiate.h"
#include "InputOutputDiff.h"
#include "getopt.h"

int main(int argc, char *argv[])
{
	try
	{
		std::vector<std::string> vs;

		bool debug = false;
		size_t context_size = 2;
		while (true)
		{
			int c;
			static struct option long_options[] =
				{
					{"context", required_argument, 0, 'C'},
					{"automaton", required_argument, 0, 'a'},
					{"help", no_argument, 0, 'h'},
					{"debug", no_argument, 0, 'd'},
					{"file-automaton", required_argument, 0, 'f'}

				};
			int option_index = 0;
			c = getopt_long(argc, argv, "C:a:hdf:", long_options, &option_index);
			if (c == -1)
			{
				break;
			}
			switch (c)
			{
			case 'f':
			{
				std::ifstream f4(optarg);
				std::string l1;
				while (std::getline(f4, l1))
				{
					vs.push_back(l1);
				}
				break;
			}
			case 'a':
				vs = utils::parse_one_line_automata(optarg);
				break;
			case 'C':
				context_size = std::stoi(optarg);
				break;
			case 'h':
				input_output_diff::printhelp();
				return 0;
			case 'd':
				debug = true;
				break;
			}
		}
		std::string input_file1, input_file2;
		if (argc <= optind)
		{
			throw std::logic_error("Need 2 files to specify");
		}
		auto a = argv[optind];
		input_file1 = a;
		if (a == nullptr)
		{
			throw std::logic_error("Need 2 files to specify");
		}

		if (vs.empty())
		{
			throw std::logic_error("Automaton not defined");
		}

		if (debug)
		{
			input_output_diff::print_tokens(input_file1, vs);
			return 0;
		}
		++optind;
		if (argc <= optind)
		{
			throw std::logic_error("Need 2 files to specify");
		}
		a = argv[optind];
		if (a == nullptr)
		{
			throw std::logic_error("Need 2 files to specify");
		}
		input_file2 = a;

		if (input_output_diff::is_file(input_file1) && input_output_diff::is_file(input_file2))
		{
			input_output_diff::compare_and_print(input_file1, input_file2, vs, context_size);
		}
		if (!input_output_diff::is_file(input_file1) && !input_output_diff::is_file(input_file2))
		{
			input_output_diff::compare_folders(input_file1, input_file2, vs, context_size);
		}

		return 0;
	}

	catch (std::exception &e)
	{
		std::cout << "ERROR: " << e.what() << std::endl;
		return 1;
	}
}
