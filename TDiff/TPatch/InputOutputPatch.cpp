#include <string>
#include "InputOutputPatch.h"
#include <fstream>
#include <vector>
#include <sstream>
#include "Tokenizer.h"
#include <experimental/filesystem>
#include <iostream>
#include "Utils.h"
#include <stdexcept>

namespace input_output_patch
{
	inline void get_next_line(std::ifstream &ifs, std::string &str)
	{
		std::getline(ifs, str);
	}
	inline void read_header(std::ifstream &ifs, std::string &actual_line, parsed_patch_file &result)
	{
		//source line
		if (actual_line.substr(0, 3) != "***")
		{
			throw std::logic_error("Wrong patch file");
		}
		auto s = actual_line.substr(4);
		auto space_pos = s.find(' ');
		auto path_str = s.substr(0, space_pos);
		result.source_path = path_str;
		result.patch_output_path = path_str;
		auto time_str = s.substr(space_pos + 1);
		bool check_last_mod = false;
		if (check_last_mod)
		{
			if (time_str != utils::get_time_modification(result.source_path))
			{
				throw std::logic_error("Wrong last modification date");
			}
		}

		get_next_line(ifs, actual_line);
		//target line
		if (actual_line.substr(0, 3) != "---")
		{
			throw std::logic_error("Wrong patch file");
		}
		s = actual_line.substr(4);
		space_pos = s.find(' ');
		path_str = s.substr(0, space_pos);
		result.target_path = path_str;
		time_str = s.substr(space_pos + 1);
		if (check_last_mod)
		{
			if (time_str != utils::get_time_modification(result.target_path))
			{
				throw std::logic_error("Wrong last modification date");
			}
		}
		get_next_line(ifs, actual_line);
		//automata
		while (actual_line.substr(0, 2) == "@@")
		{
			result.automata_definition.push_back(actual_line.substr(3));
			get_next_line(ifs, actual_line);
		}
	}
	inline void read_hunk(std::ifstream &ifs, std::string &actual_line, parsed_patch_file &result)
	{
		hunk h;
		//Read Header of hunk source
		if (actual_line.substr(0, 3) == "***" && actual_line.substr(actual_line.length() - 4) == "****")
		{
			auto pos = actual_line.find(',');
			auto start = actual_line.substr(4, pos - 4);
			auto end = actual_line.substr(pos + 1, actual_line.length() - (6 + pos));
			h.start_index_source = std::stoi(start);
			h.end_index_source = std::stoi(end);
			h.originates.append(actual_line);
			h.originates.append("\n");
		}
		else
		{
			throw std::logic_error("Wrong patch file");
		}
		get_next_line(ifs, actual_line);
		//Read Header of hunk target
		if (actual_line.substr(0, 3) == "---" && actual_line.substr(actual_line.length() - 4) == "----")
		{
			auto pos = actual_line.find(',');
			auto start = actual_line.substr(4, pos - 4);
			auto end = actual_line.substr(pos + 1, actual_line.length() - (6 + pos));
			h.start_index_target = std::stoi(start);
			h.end_index_target = std::stoi(end);
			h.originates.append(actual_line);
			h.originates.append("\n");
		}
		else
		{
			throw std::logic_error("Wrong patch file");
		}
		get_next_line(ifs, actual_line);

		h.originates.append(actual_line);
		h.originates.append("\n");
		//Read hunk Data
		while (true)
		{
			line l;
			if (actual_line.empty() || actual_line.at(2) != ' ')
			{
				goto exit_loop; // NOLINT
			}
			switch (actual_line.at(0))
			{
			case 't':
				l.type = token;
				break;
			case 'w':
				l.type = whitespace;
				break;
			default:
				goto exit_loop; // NOLINT
			}
			switch (actual_line.at(1))
			{
			case '+':
				l.from = target;
				break;
			case '-':
				l.from = source;
				break;
			case ' ':
				l.from = both;
				break;
			default:
				goto exit_loop; // NOLINT
			}
			std::string s = actual_line.substr(3);
			utils::replace_from_ws(s);
			l.str = s;
			h.lines.push_back(l);
			get_next_line(ifs, actual_line);

			h.originates.append(actual_line);
			h.originates.append("\n");
		}

	exit_loop:;
		result.hunks.push_back(h);
	}

	std::vector<parsed_patch_file> parse_context_patch(const std::string &patch_file_path)
	{
		std::vector<parsed_patch_file> result;
		std::filesystem::path p(patch_file_path);
		if (!is_regular_file(p))
		{
			//std::cout << absolute(p).string();
			throw std::runtime_error(absolute(p).string()); //nejaku inu exceptionu
		}
		std::ifstream ifs(patch_file_path);
		std::string actual_line;
		get_next_line(ifs, actual_line);
		while (ifs)
		{
			parsed_patch_file ppf;
			read_header(ifs, actual_line, ppf);
			while (actual_line == "***************")
			{
				get_next_line(ifs, actual_line);
				read_hunk(ifs, actual_line, ppf);
			}
			result.push_back(ppf);
		}
		return result;
	}

	void apply_patch(std::vector<parsed_patch_file> patch_files)
	{
		for (auto &file : patch_files)
		{
			auto automata = tokenizer::build_automata(file.automata_definition);
			tokenizer::parsed_text pt;
			const char *file1_start;
			auto size_file1 = utils::mmap_file(&file1_start, file.source_path.c_str());
			tokenizer::parse_text(file1_start, size_file1, automata, pt);

			std::vector<utils::replacer> to_replace;
			bool not_rejected = true;
			bool reject_notif = false;
			for (auto &group_diff : file.hunks)
			{
				std::vector<utils::replacer> to_replace_hunk;
				bool save_to_ws_file = false;
				size_t actual_token_in_source = group_diff.start_index_source;
				bool reject = false;
				for (auto &line : group_diff.lines)
				{

					auto last_token = pt.s_tokens.at(actual_token_in_source - 1);
					auto actual_token = pt.s_tokens.at(actual_token_in_source);
					auto last_token_end = last_token.text.begin + last_token.text.length;

					if ((line.type == whitespace && GLOB_ignorews) || (line.type == whitespace && line.from == both))
					{
						continue;
					}
					switch (line.from)
					{
					case source:
					{ //delete
						if (line.type == token)
						{
							size_t pos_start = pt.s_tokens.at(actual_token_in_source).text.begin - file1_start;
							size_t pos_end = pos_start + pt.s_tokens.at(actual_token_in_source).text.length;
							to_replace_hunk.emplace_back(pos_start, pos_end, "");
							if (line.str != std::string(pt.s_tokens.at(actual_token_in_source).text.begin, pt.s_tokens.at(actual_token_in_source).text.length))
							{
								reject = true;
								break;
							}
							actual_token_in_source++;
						}
						else
						{

							auto length = actual_token.text.begin - last_token_end;
							auto str = std::string(last_token_end, length);
							if (line.str != str)
							{
								save_to_ws_file = true;
							}
							auto pos_start = (last_token.text.begin - file1_start) + last_token.text.length;
							auto pos_end = (actual_token.text.begin - file1_start);
							to_replace_hunk.emplace_back(pos_start, pos_end, "");
						}
						break;
					}
						//add
					case target:
					{
						auto pos_start = last_token_end - file1_start;

						to_replace_hunk.emplace_back(pos_start, pos_start, line.str);
						break;
					}

					case both:
					{
						if (line.str != std::string(actual_token.text.begin, actual_token.text.length))
						{
							reject = true;
							break;
						}
						if (line.type == token)
						{
							actual_token_in_source++;
						}
						break;
					}
					default:;
					}
				}
				if (save_to_ws_file)
				{
					std::ofstream myfile;
					myfile.open("whitespace.txt", std::fstream::app);

					auto position_end = pt.s_tokens.at(actual_token_in_source).text.begin + pt.s_tokens.at(actual_token_in_source).text.length;
					auto position_start = pt.s_tokens.at(group_diff.start_index_source).text.begin;
					auto length = position_end - position_start;
					std::string s(position_start, length);
					//std::string s = pt.text.substr(pt.tokens.at(group_diff.start_index_source).position_start, pt.tokens.at(actual_token_in_source).position_end - pt.tokens.at(group_diff.start_index_source).position_start);
					myfile << s << std::endl
						   << std::endl;
					myfile.close();
				}
				if (!reject)
				{
					to_replace.insert(to_replace.end(), to_replace_hunk.begin(), to_replace_hunk.end());
				}
				else
				{
					if (!reject_notif)
					{
						std::cout << "One or more hunks rejected. Check rejects.txt for more info" << std::endl;
						reject_notif = true;
					}
					std::ofstream myfile;
					myfile.open("rejects.txt", std::fstream::app);
					if (not_rejected)
					{
						not_rejected = false;
						myfile << "*** " << file.source_path << std::endl;
						myfile << "--- " << file.target_path << std::endl;
						myfile << "@@ " << utils::concat_strings(file.automata_definition, ";") << std::endl;
					}
					myfile << "***************" << std::endl;
					myfile << group_diff.originates << std::endl;
					myfile.close();
				}
			}

			//std::reverse(to_replace.begin(), to_replace.end());
			size_t text_position = 0;
			//std::ofstream out(file.source_path.append("_temp"));
			std::stringstream out;
			//auto text_end_ptr = file1_start + size_file1;
			for (auto &&rep : to_replace)
			{
				if (text_position <= rep.from)
				{
					out.write((const char *)file1_start + text_position, rep.from - text_position);
				}
				out << rep.with;
				text_position = rep.to;

				//pt.text.replace(rep.from, rep.to - rep.from, rep.with);
			}
			out.write((const char *)file1_start + text_position, size_file1 - text_position);
			std::ofstream out_file(file.patch_output_path);
			out_file << out.rdbuf();
		}
	}
} // namespace input_output_patch
