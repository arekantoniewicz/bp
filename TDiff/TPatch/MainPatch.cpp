#include <string>
#include "InputOutputPatch.h"
#include "getopt.h"

int main(int argc, char *argv[])
{
	try
	{
		std::string output;
		bool use_output = false;
		while (true)
		{
			int c;
			static struct option long_options[] =
				{
					{"ignorews", no_argument, 0, 'i'},
					{"help", no_argument, 0, 'h'},
					{"output", no_argument, 0, 'o'},
				};
			int option_index = 0;
			c = getopt_long(argc, argv, "hio:", long_options, &option_index);
			if (c == -1)
			{
				break;
			}
			switch (c)
			{
			case 'o':
				use_output = true;
				output = std::string(optarg);
				break;

			case 'i':
				input_output_patch::GLOB_ignorews = true;
				break;
			case 'h':
				input_output_patch::printhelp();
				return 0;
			}
		}
		auto _arg = argv[optind];
		if (_arg == nullptr)
		{
			throw std::logic_error("Patch file not specified");
		}

		std::string input_file1 = _arg;
		auto a = input_output_patch::parse_context_patch(input_file1);
		if (use_output && a.size() != 1)
		{
			throw std::logic_error("Cannot use output file when using patch from directory");
		}
		if (use_output)
		{
			a[0].patch_output_path = output;
		}

		input_output_patch::apply_patch(a);

		return 0;
	}
	catch (std::exception &e)
	{
		std::cout << "ERROR: " << e.what() << std::endl;
		return 1;
	}
}
