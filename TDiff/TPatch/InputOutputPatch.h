#include <string>
#include <utility>
#include <vector>
#include <iostream>

namespace input_output_patch
{
	bool inline GLOB_ignorews = false;

	enum line_from { source, target, both };
	enum line_type { token, whitespace };
	//represents one information line in patch file. Contains whether it is addition, deletion or nothing and whether it is whitespace or not.
	class line
	{
	public:
		line() : from(), type()
		{
		}

		line(std::string & s, line_from lf, line_type lt) :str(std::move(s)), from(lf), type(lt)
		{}

		std::string str;
		line_from from;
		line_type type;
	};
	//represents one context in patch files. that means lines that are next to each other. 
	//Cointains start and end index of tokens in both source and target files and vector of lines that belongs there.
	class hunk
	{
	public:
		size_t start_index_source;
		size_t end_index_source;
		size_t start_index_target;
		size_t end_index_target;
		std::vector<line> lines;
		std::string originates;
	};
	//Represents whole patch file, paths to both files, automata definition and hunks
	class parsed_patch_file
	{
	public:
		std::string source_path;
		std::string target_path;
		std::vector<std::string> automata_definition;
		std::vector<hunk> hunks;
		std::string patch_output_path;
	};
	//gets path to patch fileparse and create parsed_patch_file.
	std::vector<parsed_patch_file> parse_context_patch(const std::string & patch_file_path);

	//takes parsed\_patch\_file and applies all changes to target file
	void apply_patch(std::vector<parsed_patch_file> patch_files);
	inline void printhelp()
	{
		std::cout << "Argument path_to_patch_file" << std::endl;
		std::cout << "-o PATH specifie file to which apply the patch" << std::endl;
		std::cout << "Automaton is defined in patch file" << std::endl;

	}
}
