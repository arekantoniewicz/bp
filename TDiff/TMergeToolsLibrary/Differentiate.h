#pragma once
#include <utility>
#include "Tokenizer.h"
#include <filesystem>
#include <iostream>

namespace differentiate
{
	namespace fs = std::filesystem;
	//Matrix with lengths with number of tokens from original texts.
	//On i,j is edit distance of tokens from first text up to i and tokens from second text up to j.
	//Described in wagner fischer
	class difference_matrix
	{
	public:
		difference_matrix(size_t source_len, size_t target_len);
		std::vector<uint32_t> matrix;
		size_t source_length;
		size_t target_length;
		size_t get_at(size_t x, size_t y);

		void set_at(size_t x, size_t y, size_t value);
		void print()
		{
			for (size_t i = 0; i < source_length; i++)
			{
				for (size_t j = 0; j < target_length; j++)
				{
					std::cout << get_at(i, j) << " ";
				}
				std::cout << std::endl;
			}
		}
	};
	//Enum with type of differences
	enum type_of_difference
	{
		insertion,
		deletion,
		substitution,
		nothing
	};

	//Describes one difference between texts.
	//Each difference is one step in backtrack algorithm.
	//Contains type of difference (insert, delete, substitute, nothing) and indexes of tokens that are compared.
	class difference
	{
	public:
		difference(type_of_difference ty, tokenizer::s_token & s, tokenizer::s_token & t, size_t token_s, size_t token_t) : token_index_source(token_s), token_index_target(token_t), type(ty), source(std::move(s)), target(std::move(t))
		{
		}
		size_t token_index_source;
		size_t token_index_target;
		type_of_difference type;
		tokenizer::s_token source;
		tokenizer::s_token target;
	};
	//All information about comparing two files.
	//Vector of differences, automata definition, paths to both files, texts of both files and parsed text from both files.
	class file_differences
	{
	public:
		std::vector<difference> diffs;
		std::vector<std::string> automata_definition;
		fs::path file_path_source;
		fs::path file_path_target;
		tokenizer::sub_text original_text_source;
		tokenizer::sub_text original_text_target;
		tokenizer::parsed_text tokens_source;
		tokenizer::parsed_text tokens_target;
	};
	//returns the name of enum
	inline std::string type_of_difference_to_str(type_of_difference t)
	{
		switch (t)
		{
		case insertion:
			return "insertion";
		case deletion:
			return "deletion";
		case substitution:
			return "substitution";
		case nothing:
			return "";
		default:
			return "";
		}
	}

	//Takes two parsed text and create difference_matrix. Wagner fisher.
	difference_matrix differentiate_tokens(tokenizer::parsed_text &parsed_text_source, tokenizer::parsed_text &parsed_text_target);

	//Take difference matrix and both parsed text and returns differences (filled file_differences)
	void find_differences(difference_matrix &difference_matrix, file_differences &fd);
} // namespace differentiate
