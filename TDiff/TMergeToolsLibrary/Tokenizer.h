#pragma once
#include <utility>
#include <vector>
#include <string>
#include <regex>
#include <map>
#include <iostream>
#include <re2/re2.h>

namespace tokenizer
{
	//Edge in automata contains regular, starting and ending node, Whether on start or on end is in regular.
	class automata_edge
	{
	public:
		std::unique_ptr<RE2> regex;
		bool on_start_in_token = false;
		bool on_end_in_token = false;
		size_t tokens_in_regex;
		size_t state_id;
		size_t edge_id;
		size_t next_state_id;
		std::string name;
	};
	//Map where key is state id and value is vector of edges starting in this state.
	class automata
	{
	public:
		std::map<size_t, std::vector<automata_edge>> states;
		size_t begin;
	};
	//Token from text, that is compared. Contains what text it is and position in original text.
	class token
	{
	public:
		token() : position_start(0), length(0), position_end(0), hash(0)
		{
		}

		token(size_t pos_start, std::string & s) : position_start(pos_start), str(std::move(s))
		{
			calc_lenght_and_end();
			hash = std::hash<std::string>{}(str);
		};
		size_t position_start;
		std::string str;
		size_t length;
		size_t position_end;
		size_t hash;
		void calc_lenght_and_end()
		{
			length = str.length();
			position_end = position_start + length;
		}
		bool same_string(token t) const
		{
			return hash == t.hash;
		}
	};
	struct sub_text
	{
		const char *begin;
		size_t length;
	};

	struct s_token
	{
		sub_text text;
		unsigned long hash;
	};

	//Original text and vector of tokens
	class parsed_text
	{
	public:
		const char *c_text;
		std::vector<s_token> s_tokens;
		bool is_everything_in_tokens()
		{
			for (size_t i = 1; i < s_tokens.size(); i++)
			{
				if (s_tokens.at(i).text.begin != s_tokens.at(i - 1).text.begin + s_tokens.at(i - 1).text.length)
				{
					return false;
				}
			}
			return true;
		}
	};
	//Gets rules for building automata as vector of strings and returns automata.
	automata build_automata(std::vector<std::string> rules);
	//Gets text and automata and returns tokenized text.
	void parse_text(const char *text, unsigned text_length, automata &automata, parsed_text &pt);
} // namespace tokenizer
