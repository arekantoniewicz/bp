#include "Utils.h"
#include <fstream>
#include <filesystem>
#include "../TDiff/InputOutputDiff.h"
#include <iomanip>
/* For the size of the file. */
#include <sys/stat.h>
/* This contains the mmap calls. */
#include <sys/mman.h>
/* These are for error printing. */
#include <errno.h>
#include <string.h>
#include <stdarg.h>
/* This is for open. */
#include <fcntl.h>
#include <stdio.h>
/* For exit. */
#include <stdlib.h>
/* For the final part of the example. */
#include <ctype.h>
#include <iostream>
#include <regex>
#include <chrono>

namespace utils
{
	// This part is from
	// https://stackoverflow.com/questions/56788745/how-to-convert-stdfilesystemfile-time-type-to-a-string-using-gcc-9
	template <typename TP>
	std::time_t to_time_t(TP tp)
	{
		using namespace std::chrono;
		auto sctp = time_point_cast<system_clock::duration>(tp - TP::clock::now() + system_clock::now());
		return system_clock::to_time_t(sctp);
	}
	// Here ends part from
	// https://stackoverflow.com/questions/56788745/how-to-convert-stdfilesystemfile-time-type-to-a-string-using-gcc-9

	namespace fs = std::filesystem;
	std::string get_time_modification(const fs::path &p)
	{
		fs::file_time_type ftime = fs::last_write_time(p);
		std::time_t tt = to_time_t(ftime);
		struct std::tm *timeinfo{};
		timeinfo = localtime(&tt);

		std::stringstream buffer;

		buffer << std::put_time(timeinfo, "%Y-%m-%d %H:%M:%S %z");

		return buffer.str();
	}

	std::vector<std::string> parse_one_line_automata(const std::string &automata)
	{
		std::string a = automata;
		std::vector<std::string> result;
		size_t last_pos = 0;
		size_t pos = 0;
		pos = a.find(';', pos);
		while (pos != std::string::npos)
		{
			if (a.at(pos - 1) == '\\')
			{
				a.erase(pos - 1, 1);
			}
			else
			{
				result.emplace_back(a.substr(last_pos, pos - last_pos));
				last_pos = pos + 1;
			}
			pos = a.find(';', pos + 1);
		}
		result.emplace_back(a.substr(last_pos));
		return result;
	}

	static void
	check(int test, const char *message, ...)
	{
		if (test)
		{
			va_list args;
			va_start(args, message);
			vfprintf(stderr, message, args);
			va_end(args);
			fprintf(stderr, "\n");
			exit(EXIT_FAILURE);
		}
	}
	size_t mmap_file(const char **file, const char *file_path)
	{
		int fd;
		/* Information about the file. */
		struct stat s;
		int status;
		/* The file name to open. */
		const char *file_name = file_path;
		/* The memory-mapped thing itself. */
		//const char *mapped;

		/* Open the file for reading. */
		fd = open(file_path, O_RDONLY);
		check(fd < 0, "open %s failed: %s", file_name, strerror(errno));

		/* Get the size of the file. */
		status = fstat(fd, &s);
		check(status < 0, "stat %s failed: %s", file_name, strerror(errno));

		/* Memory-map the file. */
		*file = (char *)mmap(0, s.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
		check(*file == MAP_FAILED, "mmap %s failed: %s",
			  file_name, strerror(errno));

		/* fd = open("/dev/zero", O_RDONLY);
		mmap((void *)(*file + s.st_size), 4096, PROT_READ, MAP_PRIVATE | MAP_FIXED, fd, 0); */
		return s.st_size;
	}

	differentiate::file_differences compare_files(const fs::path &source, const fs::path &target, const std::vector<std::string> &automata)
	{
		differentiate::file_differences fd;
		fd.file_path_source = source;
		fd.file_path_target = target;
		if (!fs::is_regular_file(fd.file_path_source) || !fs::is_regular_file(fd.file_path_target))
		{
			throw std::logic_error("Comparing files, paths doesn't exists");
		}

		fd.automata_definition = automata;

		auto at = tokenizer::build_automata(automata);

		const char *file1_start;
		const char *file2_start;
		auto size_file1 = mmap_file(&file1_start, fd.file_path_source.c_str());
		auto size_file2 = mmap_file(&file2_start, fd.file_path_target.c_str());
		fd.original_text_source = {file1_start, size_file1};
		fd.original_text_target = {file2_start, size_file2};
		tokenizer::parse_text(file1_start, size_file1, at, fd.tokens_source);
		tokenizer::parse_text(file2_start, size_file2, at, fd.tokens_target);

		differentiate::difference_matrix dm = differentiate::differentiate_tokens(fd.tokens_source, fd.tokens_target);
		differentiate::find_differences(dm, fd);

		return fd;
	}
	std::vector<diffs_group> group_diffs(const std::vector<differentiate::difference> &diffs)
	{
		std::vector<diffs_group> result;
		bool in_group = false;
		diffs_group df{};
		for (size_t i = 0; i < diffs.size(); i++)
		{
			const auto type = diffs.at(i).type;
			if (in_group)
			{
				if (type == differentiate::nothing)
				{
					result.emplace_back(df);
					in_group = false;
				}
				else
				{
					df.diffs.push_back(diffs.at(i));
				}
			}
			else
			{
				if (type != differentiate::nothing)
				{
					df = diffs_group{};
					df.start_diff_index = i;
					df.diffs.push_back(diffs.at(i));
					in_group = true;
				}
			}
		}
		return result;
	}

	bool subtext_equals(tokenizer::sub_text s1, tokenizer::sub_text s2)
	{
		if (s1.length != s2.length)
		{
			return false;
		}
		for (size_t i = 0; i < s1.length; i++)
		{
			if (*(s1.begin + i) != *(s2.begin + i))
			{
				return false;
			}
		}
		return true;
	}
} // namespace utils
