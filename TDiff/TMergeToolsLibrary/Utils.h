#pragma once
#include <string>
#include <utility>
#include <vector>
#include "Differentiate.h"
#include <iostream>
#include <sstream>
#include <filesystem>

namespace utils
{
	namespace fs = std::filesystem;

	class replacer
	{
	public:
		replacer(size_t f, size_t t, std::string w) : from(f), to(t), with(w),diff_index(0)
		{
		}
		size_t from;
		size_t to;
		bool in_conflict = false;
		std::string with;
		size_t diff_index;
		
	};

	class diffs_group
	{
	public:
		size_t start_diff_index;
		std::vector<differentiate::difference> diffs;
		bool conflict = false;
		size_t conflict_id = -1;
	};

	//Compare two files given paths to them and automata edges
	differentiate::file_differences compare_files(const fs::path &source, const fs::path &target, const std::vector<std::string> &automata);

	//Find and replace on string
	inline std::string replace_string(std::string subject, const std::string &search, const std::string &replace)
	{
		size_t pos = 0;
		while ((pos = subject.find(search, pos)) != std::string::npos)
		{
			subject.replace(pos, search.length(), replace);
			pos += replace.length();
		}
		return subject;
	}
	inline void replace_to_ws(std::string &str)
	{
		str = utils::replace_string(str, "\\", "\\\\");
		str = utils::replace_string(str, "\n", "\\n");
		str = utils::replace_string(str, "\r", "\\r");
		str = utils::replace_string(str, "\t", "\\t");
	}
	//Used when printing and ws needs to be shown
	inline void replace_to_ws_and_write(tokenizer::s_token str, std::ostream &out)
	{
		for (size_t i = 0; i < str.text.length; i++)
		{
			auto current_char = str.text.begin[i];
			if (current_char == '\\')
			{
				out.put('\\');
				out.put('\\');
			}
			else if (current_char == '\r')
			{
				out.put('\\');
				out.put('r');
			}
			else if (current_char == '\n')
			{
				out.put('\\');
				out.put('n');
			}
			else if (current_char == '\t')
			{
				out.put('\\');
				out.put('t');
			}
			else
			{
				out.put(current_char);
			}
		}
	}

	//Used when parsing and ws needs to be created
	inline void replace_from_ws(std::string &s)
	{
		s = utils::replace_string(s, "\\n", "\n");
		s = utils::replace_string(s, "\\r", "\r");
		s = utils::replace_string(s, "\\t", "\t");
		s = utils::replace_string(s, "\\\\", "\\");
	}

	//Get time of file
	std::string get_time_modification(const fs::path &p);

	//For parsing input string automata into separate rules
	std::vector<std::string> parse_one_line_automata(const std::string &automata);

	//Put hunk of diffs together.
	std::vector<diffs_group> group_diffs(const std::vector<differentiate::difference> &diffs);

	std::string inline concat_strings(const std::vector<std::string> &elements,
									  const std::string &separator)
	{
		if (!elements.empty())
		{
			std::stringstream ss;
			auto it = elements.cbegin();
			while (true)
			{
				ss << *it++;
				if (it != elements.cend())
					ss << separator;
				else
					return ss.str();
			}
		}
		return "";
	}

	bool subtext_equals(tokenizer::sub_text s1, tokenizer::sub_text s2);

	size_t mmap_file(const char **file, const char *file_path);

} // namespace utils
