#include "Tokenizer.h"
#include <sstream>
#include <iostream>
#include <re2/re2.h>
#include <re2/stringpiece.h>

namespace tokenizer
{
	//djb2 hash
	unsigned long inline hash(const char *str, size_t length)
	{
		unsigned long hash = 5381;
		int c;
		for (size_t i = 0; i < length; i++)
		{
			c = *(str + i);
			hash = ((hash << 5) + hash) + c;
		}
		return hash;
	}
	static size_t state_id = 0;
	static inline size_t get_id_from_name(std::string &s, std::map<std::string, std::size_t> &map)
	{
		if (map.find(s) == map.end())
		{
			map.emplace(s, state_id);
			state_id++;
			return state_id - 1;
		}
		else
		{
			return map[s];
		}
	}

	automata build_automata(std::vector<std::string> rules)
	{
		automata result{};
		size_t edge_id(0);
		RE2::Options re_option;
		re_option.set_log_errors(false);
		std::map<std::string, std::size_t> name_to_id;
		for (auto &def : rules)
		{
			automata_edge ae;
			const size_t string_length = def.length();
			const auto first_coma = def.find_first_of(',');
			const auto last_coma = def.find_last_of(',');
			std::stringstream ss(def.substr(0, first_coma));
			std::string name;
			ss >> name;
			ae.state_id = get_id_from_name(name, name_to_id);
			ss = std::stringstream(def.substr(last_coma + 1, string_length - last_coma));
			ss >> name;
			ae.next_state_id = get_id_from_name(name, name_to_id);
			ae.edge_id = edge_id;
			if (edge_id == 0)
			{
				result.begin = ae.state_id;
			}
			edge_id++;
			std::string parsing_string_to_regex = def.substr(first_coma + 1, string_length - (string_length - last_coma) - (first_coma + 1));
			bool fixed = false;

			ae.regex = std::make_unique<RE2>("^" + parsing_string_to_regex, re_option);
			fixed = ae.regex->ok();

			//ab)c
			if (!fixed)
			{
				ae.regex = std::make_unique<RE2>("^(" + parsing_string_to_regex, re_option);
				if (ae.regex->ok())
				{
					fixed = true;
					ae.on_start_in_token = true;
				}
			}
			//a(bc
			if (!fixed)
			{
				ae.regex = std::make_unique<RE2>("^" + parsing_string_to_regex + ")", re_option);
				if (ae.regex->ok())
				{
					fixed = true;
					ae.on_end_in_token = true;
				}
			}
			//a)b(c
			if (!fixed)
			{
				ae.regex = std::make_unique<RE2>("^(" + parsing_string_to_regex + ")", re_option);
				if (ae.regex->ok())
				{
					fixed = true;
					ae.on_end_in_token = true;
					ae.on_start_in_token = true;
				}
				else
				{
					throw std::invalid_argument("Wrong regular");
				}
			}
			result.states[ae.state_id].push_back(std::move(ae));
		}
		return result;
	}
	//Create tokens from original text and automaton
	void parse_text(const char *text_start, unsigned text_length, automata &automata, parsed_text &pt)
	{
		re2::StringPiece string_piece(text_start, text_length);
		struct s_token stoken = {text_start, (size_t)0, hash(text_start, 0)};
		pt.s_tokens.push_back(stoken);
		size_t actual_state = automata.begin;
		bool last_edge_open_end = false;
		size_t actual_vector_size = 10;
		std::vector<re2::RE2::Arg> argv(actual_vector_size);
		std::vector<re2::RE2::Arg *> args(actual_vector_size);
		std::vector<re2::StringPiece> ws(actual_vector_size);
		for (size_t i = 0; i < actual_vector_size; ++i)
		{
			args[i] = &argv[i];
			argv[i] = &ws[i];
		}
		std::vector<size_t> cycle_detection;
		while (!string_piece.empty())
		{

			bool continue_ = false;
			//All edges from current node
			for (auto &edge : automata.states[actual_state])
			{
				size_t length_remaining = string_piece.length();
				size_t number_capture_groups = edge.regex->NumberOfCapturingGroups();

				//vectors too small, resize them
				if (number_capture_groups > actual_vector_size)
				{
					actual_vector_size = number_capture_groups;
					argv.resize(actual_vector_size);
					args.resize(actual_vector_size);
					ws.resize(actual_vector_size);
					for (size_t i = 0; i < actual_vector_size; ++i)
					{
						args[i] = &argv[i];
						argv[i] = &ws[i];
					}
				}

				bool consumed = RE2::ConsumeN(&string_piece, *(edge.regex.get()), args.data(), number_capture_groups);
				//Matched, create tokens from regex capture groups
				if (consumed)
				{
					if (length_remaining == string_piece.length())
					{
						if (std::find(cycle_detection.begin(), cycle_detection.end(), actual_state) != cycle_detection.end())
						{
							throw std::logic_error("Cycle detected. Fix automaton needed");
						}
						cycle_detection.emplace_back(actual_state);
						//"WARNING Edge matched but didn't consume anything"
					}
					else
					{
						cycle_detection.clear();
					}
					if (last_edge_open_end != edge.on_start_in_token) //if not both true or false
					{
						throw std::logic_error("Bad capture groups");
					}
					if (number_capture_groups > 0)
					{
						if (last_edge_open_end && edge.on_start_in_token)
						{
							pt.s_tokens.back().text.length += ws[0].length();
						}
						else
						{
							stoken = {ws[0].data(), ws[0].length(), hash(ws[0].data(), ws[0].length())};
							pt.s_tokens.push_back(stoken);
						}
					}
					for (unsigned i = 1; i < number_capture_groups; ++i)
					{
						stoken = {ws[i].data(), ws[i].length(), hash(ws[i].data(), ws[i].length())};
						pt.s_tokens.push_back(stoken);
					}
					actual_state = edge.next_state_id;
					last_edge_open_end = edge.on_end_in_token;
					continue_ = true;
					break;
				}
			}
			if (continue_)
			{
				continue;
			}
			throw std::logic_error("No match, text still not parsed");
		}
		stoken = {pt.s_tokens.back().text.begin + pt.s_tokens.back().text.length,
				  (size_t)0, hash(pt.s_tokens.back().text.begin + pt.s_tokens.back().text.length, 0)};
		pt.s_tokens.push_back(stoken);
	}
} // namespace tokenizer
