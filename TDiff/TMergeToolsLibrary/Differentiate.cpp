#include "Differentiate.h"

namespace differentiate
{

	difference_matrix::difference_matrix(size_t source_len, size_t target_len) : source_length(source_len), target_length(target_len)
	{
		if (source_len * target_len > 4294967295) // max of unsigned int
		{
			throw std::bad_alloc(); //too much tokens
		}

		matrix = std::vector<uint32_t>(source_len * target_len);
	}

	size_t difference_matrix::get_at(size_t x, size_t y)
	{
		return matrix.at(x + y * source_length);
	}

	void difference_matrix::set_at(size_t x, size_t y, size_t value)
	{
		matrix.at(x + y * source_length) = value;
	}
	//Creates matrix
	difference_matrix differentiate_tokens(tokenizer::parsed_text &parsed_text_source, tokenizer::parsed_text &parsed_text_target)
	{
		difference_matrix dm(parsed_text_source.s_tokens.size(), parsed_text_target.s_tokens.size());
		for (size_t i = 0; i < parsed_text_source.s_tokens.size(); i++)
		{
			dm.set_at(i, 0, i);
		}
		for (size_t i = 0; i < parsed_text_target.s_tokens.size(); i++)
		{
			dm.set_at(0, i, i);
		}
		for (size_t i = 1; i < parsed_text_source.s_tokens.size(); i++)
		{
			for (size_t j = 1; j < parsed_text_target.s_tokens.size(); j++)
			{
				if (parsed_text_source.s_tokens.at(i).hash == (parsed_text_target.s_tokens.at(j)).hash)
				{
					dm.set_at(i, j, dm.get_at(i - 1, j - 1));
				}
				else
				{
					size_t min1 = dm.get_at(i - 1, j - 1) + 1;
					size_t min2 = dm.get_at(i, j - 1) + 1;
					size_t min3 = dm.get_at(i - 1, j) + 1;
					size_t min = min1 < min2 ? min1 : min2;
					min = min < min3 ? min : min3;
					dm.set_at(i, j, min);
				}
			}
		}
		return dm;
	}
	//Backtrack matrix to find edits
	void find_differences(difference_matrix &difference_matrix, file_differences &fd)
	{
		size_t x = difference_matrix.source_length - 1;
		size_t y = difference_matrix.target_length - 1;
		while (x > 0 || y > 0)
		{
			difference d(nothing, fd.tokens_source.s_tokens.at(x), fd.tokens_target.s_tokens.at(y), x, y);
			if (x == 0)
			{
				d.type = insertion;
				fd.diffs.push_back(d);
				y--;
				continue;
			}
			if (y == 0)
			{
				d.type = deletion;
				fd.diffs.push_back(d);
				x--;
				continue;
			}
			size_t min = std::min(std::min(difference_matrix.get_at(x - 1, y - 1), difference_matrix.get_at(x - 1, y)), difference_matrix.get_at(x, y - 1));
			if (min == difference_matrix.get_at(x - 1, y - 1))
			{

				if (difference_matrix.get_at(x, y) == difference_matrix.get_at(x - 1, y - 1))
				{
					x--;
					y--;
					d.type = nothing;
					fd.diffs.push_back(d);
					continue;
				}
				else
				{
					x--;
					y--;
					d.type = substitution;
					fd.diffs.push_back(d);
					continue;
				}
			}
			if (min == difference_matrix.get_at(x, y - 1))
			{
				y--;
				d.type = insertion;
				fd.diffs.push_back(d);
				continue;
			}
			if (min == difference_matrix.get_at(x - 1, y))
			{
				x--;
				d.type = deletion;
				fd.diffs.push_back(d);
				continue;
			}
		}
		tokenizer::sub_text empty = {nullptr, 0};
		tokenizer::s_token empty_token = {empty, 0};
		fd.diffs.emplace_back(nothing, empty_token, empty_token, 0, 0);
		std::reverse(fd.diffs.begin(), fd.diffs.end());
	}

} // namespace differentiate
