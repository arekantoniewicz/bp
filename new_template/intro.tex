\chapwithtoc{Introduction}

% Introduction should answer the following questions, ideally in this order:
% \begin{enumerate}
% \item What is the nature of the problem the thesis is addressing?
% \item What is the common approach for solving that problem now?
% \item How this thesis approaches the problem?
% \item What are the results? Did something improve?
% \item What can the reader expect in the individual chapters of the thesis?
% \end{enumerate}

% Expected length of the introduction is between 1--4 pages. Longer introductions may require sub-sectioning with appropriate headings --- use \texttt{\textbackslash{}section*} to avoid numbering (with section names like `Motivation' and `Related work'), but try to avoid lengthy discussion of anything specific. Any ``real science'' (definitions, theorems, methods, data) should go into other chapters.
% \todo{You may notice that this paragraph briefly shows different ``types'' of `quotes' in TeX, and the usage difference between a hyphen (-), en-dash (--) and em-dash (---).}

% It is very advisable to skim through a book about scientific English writing before starting the thesis. I can recommend `\citetitle{glasman2010science}' by \citet{glasman2010science}.

Text comparison is an essential part of working with computers. Not only programmers but also many other professions use text comparison tools on a daily basis. There exist many different file and text comparison tools~\cite{filecomparisontools}. The text comparison tools include finding and showing differences, as well as revisiting, modifying and applying changes, comparing and showing differences in three files, merging three files together and many other cases of usage.

%The aim of this thesis is to design and implement tools related to finding differences between files (diff utilities). Diff, patch, diff3 and other are usually involved in diff utilities. The usage of the tools (diff, patch and diff3) is comparing files, modifying and applying changes, comparing 3 files and merging them together. In addition, other tools like sdiff and word diff exist. They are used by programmers on a daily basis for comparing different versions of a program. Comparing files is crucial part of version control systems and therefore diff utilities are involved in almost all of the version control systems.

There is a problem with all common existing solutions. They do not allow a user to choose the unit of comparison, whether it may be a section, sentence, word, cells in a table or anything else. By considering a text with multiple changes in a long section we can present why common existing solutions fail to provide a convenient way of working with them. GNU Diff~\cite{diffutilsmanual} (shown in \Cref{fig:diffsection}) simply shows that the lines are different but does not point to a place in the section where the difference is. Longer sections would make diff inapplicable. GNU Wdiff~\cite{gnuwdiff} (shown in \Cref{fig:wdiffsection}) demonstrates the difference in a more profound matter then the diff, however it lacks tools to patch and merge. Other solutions such as Beyond Compare~\cite{beyondcompare} or the git diff~\cite{gitdiff} with word-diff=color option (shown in \Cref{fig:wgitiffsection}) are capable of patching and showing the difference well. However, there is still a problem with merging. Neither of those tools would be able to three way merge if one file had changes at the beginning of a section and the other file had changes at the end of the same section.

The aim of this thesis is to design and implement tools that can work with various file formats, print readable differences and apply them. To be capable of working with many different formats, the user needs to be able to divide the text into sections of their own accord, which then they compare to each other. The process of text division is called tokenization and the results are called tokens.

The implemented utilities should be able to tokenize texts using rules defined by the user, work with the tokenized text effectively and show readable differences between them. This results in a multipurpose tool of comparing any text file format.

Notably, the custom tokenization creates a problem not present in other diff implementations. When a part of the text is left untokenized, it is considered as a whitespace which is not significant for comparing. However, a whitespace around the tokens may sometimes carry information that is relevant for the result, and thus needs to be handled separately.

\section*{Layout of this Thesis}
This thesis is structured as follows: the first chapter provides a detailed overview on handling text differences. There is described how to compare text, how to apply patches and how to compare and merge three files. The second chapter outlines the proposed solution for user-specified tokenization, how to implement it and what the lexers are in general. Whitespace handling is also addressed in the second chapter. The attention of the third chapter is focused on the program structure, tokenizer itself, patch format specification and the performance of the implemented tools.


\begin{figure}[]
    %   \includegraphics[scale=0.5]{img/section_diff.png}
    \begin{scriptsize}
        \begin{Verbatim}[commandchars=\\\{\},breaklines=true,breaksymbolleft=]
            *** t1	2020-07-12 11:26:02.268930863 +0200
            --- t2	2020-07-12 11:26:01.728660862 +0200
            ***************
            *** 1,2 ****
            ! In mathematical theory, linguistics and computer science, the Levenshtein distance is a string metric for measuring the difference between two sequences. Informally, the Levenshtein distance between two words is the minimum number of single-character edits (insertions, deletions or substitutions) required to change one word into the other. It is named after the Russian mathematician Vladimir Levenshtein, who considered this distance in 1965.
            ! Levenshtein distance may also be referred as edit distance, although that term may also denote a larger family of distance metrics known collectively as edit distance. It is not closely related to pairwise string alignments.
            --- 1,2 ----
            ! In information theory, linguistics and computer science, the Levenshtein distance is a string metric for measuring the difference between two sequences. Informally, the Levenshtein distance between two words is the minimum number of single-character edits (insertions, deletions or substitutions) required to change one word into the other. It is named after the Soviet mathematician Vladimir Levenshtein, who considered this distance in 1965.
            ! Levenshtein distance may also be referred to as edit distance, although that term may also denote a larger family of distance metrics known collectively as edit distance. It is closely related to pairwise string alignments.
        \end{Verbatim}
    \end{scriptsize}
    %    \fi
    \caption{GNU diff used on a text with multiple changes in the same section.}
    \label{fig:diffsection}
\end{figure}


\begin{figure}
    \begin{scriptsize}
        \begin{Verbatim}[commandchars=\\\{\},breaklines=true,breaksymbolleft=]
            In [-mathematical-] {+information+} theory, linguistics and computer science, the Levenshtein distance is a string metric for measuring the difference between two sequences. Informally, the Levenshtein distance between two words is the minimum number of single-character edits (insertions, deletions or substitutions) required to change one word into the other. It is named after the [-Russian-] {+Soviet+} mathematician Vladimir Levenshtein, who considered this distance in 1965.
            Levenshtein distance may also be referred {+to+} as edit distance, although that term may also denote a larger family of distance metrics known collectively as edit distance. It is [-not-] closely related to pairwise string alignments.
        \end{Verbatim}
    \end{scriptsize}
    \caption{GNU wdiff used on a text with multiple changes in the same section.}
    \label{fig:wdiffsection}
\end{figure}

\begin{figure}
    \begin{scriptsize}
        \begin{Verbatim}[commandchars=\\\{\},breaklines=true,breaksymbolleft=]
            @@ -1,2 +1,2 @@
            In \textcolor{red}{mathematical}\textcolor{green}{information} theory, linguistics and computer science, the Levenshtein distance is a string metric for measuring the difference between two sequences. Informally, the Levenshtein distance between two words is the minimum number of single-character edits (insertions, deletions or substitutions) required to change one word into the other. It is named after the \textcolor{red}{Russian}\textcolor{green}{Soviet} mathematician Vladimir Levenshtein, who considered this distance in 1965.
            Levenshtein distance may also be referred\textcolor{green}{ to} as edit distance, although that term may also denote a larger family of distance metrics known collectively as edit distance. It is\textcolor{red}{not} closely related to pairwise string alignments.
        \end{Verbatim}
    \end{scriptsize}
    \caption{GIT diff with enabled word diff color option used on a text with multiple changes in the same section.}
    \label{fig:wgitiffsection}
\end{figure}