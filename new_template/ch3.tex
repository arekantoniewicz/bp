\chapter{Implementation and results}

Three utilities, TDiff, TPatch and TDiff3, have been implemented. They were programmed using the C++ programming language and the C++20 standard. Proof of concept of patching files without collisions in whitespaces is implemented in TPatch. TDiff3 supports token-by-token patching which is sufficient in most cases. In this chapter, the structure of the program and its implementation details are going to be introduced. After that we are going to show the results of a benchmark to see if it can handle larger files in reasonable time. At the end, we are going to show many different aplications where having a user specifiable tokenization in the diff is superior to almost all the other diff utilities.
 

\section {Program structure}
The program is divided into 3 standalone executables and one library which is used by all the projects. In every project there is a Main file and an InputOutput file. The Main is used for parsing arguments and calling appropriate methods from the InputOutput file. In the InputOutput file there is a logic of the program and it is calling the shared library methods. The program structure can be seen in \Cref{fig:structure}.

\begin{figure}
    \begin{tikzpicture}[thick,scale=0.9, every node/.style={scale=0.9}]
        \node[draw,ultra thick,label={\textbf{Shared library}},align=left] at (10,-3) (lib) { Tokenize text \\  Compute levensthein matrix \\ Find edits operation in matrix};

        \node[ultra thick] at (1.5,1.5) (diff){\textbf{TDiff}};
        \node[draw,ultra thick,label=Main] at (0,0) (diffmain){ Parse arguments};
        \node[draw,ultra thick,label=InputOutput,align=left] at (4,0) (diffio){ Find diff \\ Print diff};

        \draw[->] (diffmain) -- (diffio) node [midway, above, sloped] () {};
        \draw[->] (diffio.east) -- (lib.north west) node [midway, above, sloped] () {};


        \node[ultra thick] at (1.5,-1.5) (patch){\textbf{TPatch}};
        \node[draw,ultra thick,label=Main] at (0,-3) (patchmain){Parse arguments};
        \node[draw,ultra thick,label=InputOutput,align=left] at (4,-3) (patchio){ Parse patch file \\ Apply patch file};


        \draw[->] (patchmain) -- (patchio) node [midway, above, sloped] () {};
        \draw[->] (patchio) -- (lib.west) node [midway, above, sloped] () {};

        \node[ultra thick] at (1.5,-4.5) (diff3){\textbf{TDiff3}};
        \node[draw,ultra thick,label=Main] at (0,-6) (diff3main){ Parse arguments};
        \node[draw,ultra thick,label=InputOutput,align=left] at (4,-6) (diff3io){ Find diff in 3 files \\ Merge};

        \draw[->] (diff3main) -- (diff3io) node [midway, above, sloped] () {};
        \draw[->] (diff3io.east) -- (lib.south west) node [midway, above, sloped] () {};

    \end{tikzpicture}
    \caption{The program structure.}
    \label{fig:structure}
\end{figure}

\subsection{Shared library}
The shared library contains 3 header files and their implementation.
\begin{itemize}
    \item Tokenizer

          The Tokenizer is used for parsing the text into tokens. There are classes used for the definition of REDFA and its edges. It also contains the definition of the parsed text which consists of the tokens and the text that was tokenized. Methods for building the REDFA from the rules and for tokenizing the text with the REDFA are implemented as well.

    \item Differentiate

          The Differentiate is used for finding the differences in two tokenized texts. It contains the definition of edit distance matrix, file differences and the difference. The file difference contains information about differentiating two files, paths to those files, their contents, the tokenized content and differences between them. One difference contains its type and indexes of tokens that are compared. There are methods for the Wagner and Fischer algorithm to create a matrix from the tokens and to backtrack the matrix to find the differences.

    \item Utils

          The Utils contains other methods and classes --- a method that takes two paths to files and an REDFA definition, creating an REDFA, reading files, comparing files and returning them filled into the file difference class, a method for replacing whitespaces (tab, newline) for printable characters and vice versa, a method for splitting one argument which contains all the rules of REDFA into separate rules and a method to group the diffs that are near each other within the context.

\end{itemize}

\subsection{TDiff}
TDiff is a program which compares two files or directories according to the given rules and prints the differences to a standard output. The Main parses arguments and calls the methods from InputOutput file. The InputOutput contains a method for comparing folders and files and for printing differences.

The format for executing the diff is \texttt{tdiff from to [options]}. From and to must be paths to either files or directories.

Below, there is a summary of all the options that the Token-aware diff accepts. Standard long and short arguments can be passed to the diff. The Getopt~\cite{getopt} is used to parse the input arguments.

\begin{tabular}{lL}
    {-}{-}automaton, -a AT         & Use AT to build an automaton to tokenize the input. The rules are in a format starting state, regular expression, ending state. The rules are separated by a semicolon. The semicolon in the rules needs to be escaped with a backslash. \\
    {-}{-}context, -C NUM          & Output NUM (default 3) lines of context.                                                                                                                                                                                  \\
    {-}{-}debug, -d               & Tokenize file and print the tokens.                                                                                                                                                                                      \\
    {-}{-}help, -h                & Display help.                                                                                                                                                                                                              \\
    {-}{-}file-automaton, -f PATH & Read the rules from PATH. The rules are delimited by newlines.
\end{tabular}

%Skip common prefix and sufix tokens. Starts comparing number\_of\_tokens tokens before the first different token and number\_of\_tokens tokens after the last one.





\subsection{TPatch}

TPatch is a program which applies patches on a file. The input is a patch file which contains information about which file is going to be patched and what changes are going to be applied. It applies the changes, rejected changes and whitespaces that were deleted are then saved to separate files. The Main parses arguments and calls methods from the InputOutput file. The InputOutput contains a method to parse the patch file and apply the changes.

The format for running the patch is \texttt{tpatch patch-file [options]}. The patch file must be in the format described further below.

\begin{tabular}{lL}
    {-}{-}ignorews, -i     & Ignore whitespaces during patching. Only the tokens are inserted and deleted. \\
    {-}{-}output, -o PATH & Write output to PATH instead of the path specified in the patch.                  \\
    {-}{-}help, -h        & Display help.                                                                \\
\end{tabular}

\subsection{TDiff3}

TDiff3 is a program that reads 3 files and merges them together. The Main parses arguments and calls the method from the InputOutput file. The InputOutput contains a method of comparing three files, merging them and to printing the result.

The format for running the diff3 is \texttt{tdiff3 mine base yours [options]}. Mine, base and yours are the paths to three files.

\begin{tabular}{lL}
    {-}{-}automaton, -a AT         & Use AT to build an automaton to tokenize the input. The rules are in a format starting state, regular expression, ending state. The rules are separated by a semicolon. The emicolon in the rules needs to be escaped with a backslash. \\
    {-}{-}file-automaton, -f PATH & Read the rules from PATH. The rules are delimited by newlines.                                                                                                        \\
    {-}{-}help, -h                & Display help.                                                                                                                                                                                                          \\
\end{tabular}

\section{Data formats}

\subsection{Tokenizer specification}

To define the REDFA we use something that has already been mentioned in \Cref{subs:redfadef}. The REDFA is going to be defined as an ordered set of rules (edges). Each rule has a starting state, regular expression and an ending state. To tokenize the input, capture groups are used. It is possible for a capture group not to end in one edge. The token starts in one edge and ends in another one. This is achieved by allowing the regular expression to have an incomplete group structure. As an example we consider rules \texttt{1,a(bc,2;2,d)aa,1} and input \texttt{abcdaa}. The tokenization is going to be successful with the application of these rules and the input will result in one token \texttt{bcd}.

\subsection{Patch file format}

The output of the diff can be seen in \Cref{fig:difformat}. On the first two lines there is a path to the source and the target files. On the third line there is a definition of the REDFA delimited by newlines. After that there are hunks. All hunks start with a line consiting of asterisks. It is followed by two lines with indexes of tokens of a source and a target used in the hunk. The hunk is similar to the hunk in GNU patch.

When a line should be deleted there is a minus at the beginning of the line. The same is applied with a plus and addition of a line. Space means that everything is on a place where it is supposed to be and it remains as it is. In the token-aware patch there are tokens and whitespaces instead of the lines but the notions are the same in both patches. To differentiate between the whitespaces and the tokens we use letters \texttt{'w'} and \texttt{'t'}. After \texttt{'w'} or \texttt{'t'} we put \texttt{'+'}, \texttt{'-'} or \texttt{' '} as a second character to determine the type of the operation in the patch. All these can be seen in \Cref{fig:difformat}.

\begin{figure}
    \includegraphics[scale=0.7]{img/diffformat.jpg}
    \caption{Example of a token-aware diff tokenized into lines.}
    \label{fig:difformat}
\end{figure}


\section{Performance and use cases}
\subsection{Tokenization performance}
The benchmarks are done using Ubuntu 18.04.4 on the virtual machine with the host running Windows 10 1903. The tool for measurements is linux \texttt{time}~\cite{time} utility. Presented results are always mean time of ten runs with the slowest run being discarded. We are going to redirect the output to /dev/null as writing output can add overhead. The text will be generated lorem ipsum.

To measure the tokenization time we are going to use the diff debug option. With this option only tokenization is going to run. The results can be seen in \Cref{tab:tokenbenchmark}. The results suggest what we expected:
\begin{itemize}
    \item For the same REDFA, execution time depends linearly on the length of the text.
    \item For almost the same REDFA (only capture group changed), the execution time depends on the number of tokens.
    \item Regular expressions definitions can heavily affect the performance.
\end{itemize}
To sum up, the tokenization runs reasonably quickly even on larger files, however, not optimal definition of REDFA (both regular expression itself as well as DFA) can slow down a great deal of the execution time.



\begin{table}
    \centering
    \small
    \begin{tabular}{llr}
        File size                           & REDFA definition                                                    & Mean time \\
        \toprule
        200000 words\textasciitilde 1.4MB   & \multirow{2}{*}{1,(\textbackslash S*\textbackslash s*),1}              & 0.125s    \\
        2000000 words\textasciitilde 13.5MB &                                                                        & 1.274s    \\
        \midrule
        200000 words\textasciitilde 1.4MB   & \multirow{2}{*}{ \shortstack{1,(\textbackslash S*\textbackslash s*),2;             \\ 2,\textbackslash S*\textbackslash s*,1}} & 0.088s \\
        2000000 words\textasciitilde 13.5MB &                                                                        & 0.864s    \\

        \midrule
        200000 words\textasciitilde 1.4MB   & \multirow{2}{*}{ \shortstack{1,(\textbackslash S+),1;                              \\ 1,\textbackslash s*,1}}              & 0.190s    \\
        2000000 words\textasciitilde 13.5MB &                                                                        & 1.901s    \\
    \end{tabular}
    \caption{The tokenization benchmark.}
    \label{tab:tokenbenchmark}
\end{table}


\subsection{Use case}
First, let us compare the long section we have already mentioned in the intro. As we can see in \Cref{fig:gnuandtdiff}, both GNU diff and tdiff produce precise and readable output, however only tdiff is capable of running tpatch in this format.


\begin{figure}
    \begin{minipage}{.45\textwidth}
        \begin{scriptsize}
            \begin{Verbatim}[commandchars=\\\{\},breaklines=true,breaksymbolleft=]
                In [-mathematical-] {+information+} theory, linguistics and computer science, the Levenshtein distance is a string metric for measuring the difference between two sequences. Informally, the Levenshtein distance between two words is the minimum number of single-character edits (insertions, deletions or substitutions) required to change one word into the other. It is named after the [-Russian-] {+Soviet+} mathematician Vladimir Levenshtein, who considered this distance in 1965.
            \end{Verbatim}
        \end{scriptsize}

    \end{minipage}
    \begin{minipage}{.45\textwidth}
        \begin{scriptsize}
            \begin{Verbatim}[commandchars=\\\{\},breaklines=true,breaksymbolleft=]
                *** 1,4 ****
                --- 1,4 ----
                t  In
                t- information
                t+ mathematical
                t  theory,
                t  linguistics
                ***************
                *** 51,55 ****
                --- 51,55 ----
                t  after
                t  the
                t- Soviet
                t+ Russian
                t  mathematician
                t  Vladimir
            \end{Verbatim}
        \end{scriptsize}
    \end{minipage}
    \caption{GNU wdiff and tdiff.}
    \label{fig:gnuandtdiff}
\end{figure}






Next, let us show a simple case where patching using the GNU utilities fails, but tdiff and tpatch are able to handle it. Firstly, let us consider two simple c files with small changes (as can be seen in \Cref{fig:twoc}). Then we run diff between this two files to produce the patching file. The outputs of diffs can be seen in \Cref{fig:difssout}. As we can observe, the tdiff output is more verbose. Let us see what happens when we change the formatting of the source file (the file we are applying the patch to). The changed file and the tpatch result can be seen in \Cref{fig:appliedtpatch}. The GNU patch fails to apply anything to the changed file, on the other hand, the tpatch handles it correctly.

\input{ch3-noformat.tex}

Another example we can offer is a three-way merge. Considering two files already mentioned earlier in \Cref{fig:twoc}, we add the third file to those 2 with changed \texttt{Hello world} to \texttt{Hi}. The GNU Diff3 fails to produce merged output of these 3 files. However, the tdiff3 is capable of doing a correct merge as can be seen in \Cref{fig:diff3output}.

\section{GIT integration}
To use the utilities with GIT, it is easy to configure git difftool and git mergetool. One of many ways to make it work can be seen in \Cref{fig:gitconf}
\iffalse
\texttt{git config --global diff.tool tdiff}

\texttt{git config --global difftool.tdiff.cmd "/path/to/tdiff \textbackslash\$LOCAL \textbackslash\$REMOTE -f /path/to/file/with/definitions"}

\texttt{git config --global difftool.tdiff.trustExitCode false}



\texttt{git config --global merge.tool tdiff3}

\texttt{git config --global mergetool.tdiff3.cmd "/path/to/tdiff3 \textbackslash\$LOCAL \textbackslash\$BASE \textbackslash\$REMOTE -f /path/to/file/with/definitions"}

\texttt{git config --global mergetool.tdiff3.trustExitCode false}
\fi
\begin{figure}
    

\begin{verbatim}
git config --global diff.tool tdiff
git config --global difftool.tdiff.cmd "/path/to/tdiff
    \$LOCAL \$REMOTE -f /path/to/file/with/definitions"
git config --global difftool.tdiff.trustExitCode false

git config --global merge.tool tdiff3
git config --global mergetool.tdiff3.cmd "/path/to/tdiff3
    \$LOCAL \$BASE \$REMOTE -f /path/to/file/with/definitions"
git config --global mergetool.tdiff3.trustExitCode falses
\end{verbatim}
\caption{GIT configuration with utilities.}
\label{fig:gitconf}
\end{figure}