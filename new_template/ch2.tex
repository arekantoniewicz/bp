\chapter{Custom tokenization support} \label{chapter2}

There are many different versions of diffs. They differ in the application and the way of displaying changes. Some of them are designed for finding differences in specific file formats. HTML diff tries to compare not only the source codes but also the appearance of the final webpage. XML diffs compare the hierarchical structure of XML documents. There is a word comparing option in the Gits implementation of the diff (as seen in \Cref{fig:gitwdiff}) but it lacks the patch. None of the tools mentioned above allow the user to specify the tokenization. The user specifiable tokenization has an advantage in its wide variety of applications. It can result in a universal diff that could then handle any programming language and text format.

There are many possible existing solutions for the user-specifiable implementation of the tokenization process: regular expressions with capture groups, lexical analyzer generators such as the Lex and the Flex. In this thesis we are going to design and implement our own solution.

Terms such as automaton, regular grammar, (non)deterministic finite state machine etc. are used in this section. Their definitions can be found in a book by~\citet{automatatheory}.


\begin{figure}
  \includegraphics[scale=0.5]{img/git_word_diff.jpg}
  \caption{The git diff with enabled word option.}
  \label{fig:gitwdiff}
\end{figure}


\section{Lexing specification}
Most tokenizers are designed to use a regular grammar. Tokenizers are sometimes referred to as lexers. Although they share very similar properties, the difference between them is that a lexer usually attaches an extra context to the tokens.  We are going to consider regular expression~\cite{automatatheory} and try to simplify the defining of a more complex tokenization using a deterministic finite state machine with regular expression as edges. Let us call it Regex Edge Deterministic Finite Automaton (REDFA). The definition of REDFA is similar to the definition of the deterministic finite automaton but with an elaborate transition function. An example of REDFA can be seen in \Cref{fig:automata}.


\begin{defn}
  Definition of Regex Edge Deterministic Finite Automaton (REDFA) $M$ is a 6-tuple, $(Q,\Sigma, R, \gamma,q_0,F)$, consisting of:
  \begin{itemize}
    \item a finite set of states  $Q$
    \item a finite set of input symbols called the alphabet $\Sigma$
    \item a finite set of regular expressions, search patterns over alphabet $R$
    \item a finite set of tuples $(q_1,r,1_2)$ $\gamma$, where $q_1,q_2 \in Q$ and $r \in R$
    \item an initial state $q_0 \in Q$
    \item a set of accept states $F \subseteq Q$
  \end{itemize}
  Let $w,a_1,a_2,\dots, a_n$ be strings over the alphabet $\Sigma$. $w = a_1a_2\dots a_n$. The automaton $M$ accepts the string $w$ if a sequence of states, $s_{0},s_{1},\dots,s_{n}$, exists in $Q$ with the following conditions:
  \begin{enumerate}
    \item $r_0 = q_0$
    \item $r \in R:r$ accepts (is matching) $a_i \wedge (a_i,r,a_{i+1}) \in \gamma$
    \item $r_n \in F$
  \end{enumerate}
\end{defn}

\begin{thm}
  Any language accepted by REDFA is a regular language.
\end{thm}

\begin{figure}
  \centering
  \begin{tikzpicture}
    \node[circle,draw,ultra thick] at (0,2) (s1){\small state1};
    \node[circle,draw,ultra thick] at (4,0) (s2){\small state2};
    \node[circle,draw,ultra thick,double] at (4,4) (s3){\small state3};
    \node[circle,draw,ultra thick] at (8,2) (s4){\small state4};
    \draw[->] (s1) -- (s2) node [midway, above, sloped] (TextNode1) {\small a(b|c)};
    \draw[->] (s1) -- (s3) node [midway, above, sloped] (TextNode2) {\small a*b};
    \draw[->] (s2) -- (s4) node [midway, above, sloped] (TextNode3) {\small (c|a)\{2\}};
    \draw[->] (s4) -- (s3) node [midway, above, sloped] (TextNode4) {\small abc};
    \draw[->] (s3) -- (s2) node [midway, above, sloped] (TextNode4) {\small a*c*b};
  \end{tikzpicture}
  \caption{The state machine where edges are regular expressions.}
  \label{fig:automata}
\end{figure}

\begin{proof}
  At first, let us prove that a finite state machine with the edges as regular expression still fulfills the criteria for being a finite state machine.

  We start with converting the regular expression to a NFA (nondeterministic finite automaton). This is called the Thompson algorithm~\cite{thompson}. The algorithm works recursively by splitting an expression into its constituent subexpressions, from which the NFA will be constructed using a set of rules. The constants and operations, which define a basis for the construction of the regular expression, are going to be used. The elementary constants are an empty expression $\epsilon$ and an expression with one symbol of alphabet. Operations are a union expression~$|$, a concatenation expression and The Kleene star expression~$*$. Firstly we convert elementary constants (as seen in \Cref{fig:elem}) and expand the constants with regular expression operations (as seen in \Cref{fig:basicregexp}). Now we have an NFA instead of a regular expression. We can replace all regular expression edges in our REDFA with the NFA. Then we create an epsilon edge (empty expression $\epsilon$) from the starting node of REDFA edge to the starting node of NFA created from the regular expression and also create epsilon edges starting in all the ending nodes of the created NFA to the ending node of the REDFA edge.

  As for the next step, ordered edges are transformed to be a part of a nondeterministic finite-state machine, starting from the highest priority to the lowest priority edge for each node, unioning the complement with all edges with lower priority. The complement of state machine is done by reversing accepting and non accepting states. The complement of a regular expression \texttt{a(b|c)} is shown in \Cref{fig:negation}.
\end{proof}

\begin{figure}
  \centering
  \begin{tikzpicture}
    \node[circle,draw,ultra thick] at (0,0) (s1){\small 1};
    \node[circle,draw,ultra thick,double] at (2,0) (s2){\small 2};
    \node[align=center] at (1,1.5) {NFA representing an empty string};
    \draw[->] (s1) to[out=45,in=135] node [midway, above, sloped] {\small $\epsilon$} (s2) ;
  \end{tikzpicture}
  \begin{tikzpicture}
    \node[circle,draw,ultra thick] at (0,0) (s1){\small 1};
    \node[circle,draw,ultra thick,double] at (2,0) (s2){\small 2};
    \node[align=center] at (1,1.5) {NFA representing \texttt{a}};
    \draw[->] (s1) to[out=45,in=135] node [midway, above, sloped] {\small $a$} (s2) ;
  \end{tikzpicture}
  \caption{Elementary constants --- empty and one character long.}
  \label{fig:elem}
\end{figure}

\begin{figure}
  \centering
  \begin{tikzpicture}
    \node[circle,draw,ultra thick] at (0,0) (s1){\small 1};
    \node[circle,draw,ultra thick,double] at (2,0) (s2){\small 2};
    \node[align=center] at (1,1.5) {The union operator \texttt{a|b}};
    \draw[->] (s1) to[out=45,in=135] node [midway, above] {\small $a$} (s2) ;
    \draw[->] (s1) to[out=-45,in=-135] node [midway, below] {\small $b$} (s2) ;
  \end{tikzpicture}
  \begin{tikzpicture}
    \node[circle,draw,ultra thick] at (0,0) (s1){\small 1};
    \node[circle,draw,ultra thick] at (2,0) (s2){\small 2};
    \node[circle,draw,ultra thick,double] at (4,0) (s3){\small 3};
    \node[align=center] at (2,1.5) {Concatenation \texttt{ab}};
    \draw[->] (s1) to[out=45,in=135] node [midway, above, sloped] {\small $a$} (s2) ;
    \draw[->] (s2) to[out=45,in=135] node [midway, above, sloped] {\small $b$} (s3) ;
  \end{tikzpicture}
  \begin{tikzpicture}
    \node[circle,draw,ultra thick] at (0,0) (s1){\small 1};
    \node[circle,draw,ultra thick] at (2,0) (s2){\small 2};
    \node[circle,draw,ultra thick] at (4,0) (s3){\small 3};
    \node[circle,draw,ultra thick,double] at (6,0) (s4){\small 4};
    \node[align=center] at (2,1.5) {The Kleene closure \texttt{a*}};
    \draw[->] (s1) to[out=45,in=135] node [midway, above] {\small $\epsilon$} (s2) ;
    \draw[->] (s2) to[out=45,in=135] node [midway, above] {\small $a$} (s3) ;
    \draw[->] (s3) to[out=45,in=135] node [midway, above] {\small $\epsilon$} (s4) ;
    \draw[->] (s3) to[out=-135,in=-45] node [midway, above] {\small $\epsilon$} (s2) ;
    \draw[->] (s1) to[out=-45,in=-135] node [midway, below] {\small $\epsilon$} (s4) ;
  \end{tikzpicture}
  \caption{Converting a basic regular expression operator into NFA.}
  \label{fig:basicregexp}
\end{figure}


\begin{figure}
  \centering
  \begin{tikzpicture}[scale=0.9]
    \node[circle,draw,ultra thick,minimum width = 1.5cm] at (0,3) (s1){\scriptsize state 1};
    \node[circle,draw,ultra thick,minimum width = 1.5cm] at (12,3) (s2){\scriptsize state 3};
    \node[circle,draw,ultra thick,minimum width = 1.5cm,align = center] at (3,3) (s3){\scriptsize starting \\ \scriptsize state};
    \node[circle,draw,ultra thick,minimum width = 1.5cm,align = center] at (6,3) (s4){\scriptsize state 2};
    \node[circle,draw,ultra thick,minimum width = 1.5cm,align = center] at (9,1) (s5){\scriptsize ending \\ \scriptsize state};
    \node[circle,draw,ultra thick,minimum width = 1.5cm,align = center] at (9,5) (s6){\scriptsize ending \\ \scriptsize state};
    \draw[->] (s1) -- (s3) node [midway, above, sloped] (TextNode1) { $\epsilon$};
    \draw[->] (s3) -- (s4) node [midway, above, sloped] (TextNode1) { a};
    \draw[->] (s4) -- (s5) node [midway, above, sloped] (TextNode1) { b};
    \draw[->] (s4) -- (s6) node [midway, above, sloped] (TextNode1) { c};
    \draw[->] (s5) -- (s2) node [midway, above, sloped] (TextNode1) { $\epsilon$};
    \draw[->] (s6) -- (s2) node [midway, above, sloped] (TextNode1) { $\epsilon$};
  \end{tikzpicture}
  \caption{Inserting a regular expression \texttt{a(b|c)} into state machine.}
  \label{fig:regextoauto}
\end{figure}


\begin{figure}
  \centering
  \begin{tikzpicture}
    \node[circle,draw,ultra thick,minimum width = 1.5cm,align = center,double] at (3,3) (s3){\small s1};
    \node[circle,draw,ultra thick,minimum width = 1.5cm,align = center] at (6,3) (s4){\small s2};
    \node[circle,draw,ultra thick,minimum width = 1.5cm,align = center] at (9,1) (s5){\small s3};
    \node[circle,draw,ultra thick,minimum width = 1.5cm,align = center] at (9,5) (s6){\small s4};
    \draw[->] (s3) -- (s4) node [midway, above, sloped] (TextNode1) {\small a};
    \draw[->] (s4) -- (s5) node [midway, above, sloped] (TextNode1) {\small b};
    \draw[->] (s4) -- (s6) node [midway, above, sloped] (TextNode1) {\small c};
  \end{tikzpicture}
  \caption{The complement of a regular expression \texttt{a(b|c)} converted into an NFA.}
  \label{fig:negation}
\end{figure}

A regular finite state machine with ordered edges and edges as regular expressions (REDFA) is proven to accept regular language.

\subsection{Specifying REDFA using strings}
\label{subs:redfadef}
Let us show how REDFA can be used to make user-specifiable tokenization easier. The usage is similar to using a simple regular expression. Capture groups are used to define tokens and anything that is not in any capture group is considered a whitespace.

Before showing the differences of a simple regular expression and REDFA we need to decide how to define REDFA. We can easily define REDFA by specifying a set of 3-tuples $\gamma$, 3-tuples consisting of (starting node, regular expression, ending node). This is sufficient to define REDFA:
\begin{itemize}
  \item $Q$ --- All nodes in rules
  \item Alphabet $\Sigma$ --- same as the alphabet regular expression use
  \item $R$ --- All regular expressions in rules
  \item $\gamma$ --- It is the same as rules
  \item $q_0$ --- Starting node of the first rule
  \item $F$ --- All nodes ($F=Q$)
\end{itemize}



Simple regular expression is shorter but it becomes unreadable in more complicated rules. On the other hand, REDFA definitions are easily readable and extendable. It is possible to use REDFA as a simple regular expression --- an automaton with only one node and an edge going into itself.

Examples can be seen in \Cref{fig:textdef}.

\begin{figure}
\begin{itemize}
  \item Text definition of REDFA for words

\texttt{word,(\textbackslash{}s*),whitespace}

\texttt{whitespace,\textbackslash{}S*,word}

  \item Text definition of REDFA for CSV with header line
  
  \texttt{header,.*\textbackslash{}r\textbackslash{}n,token}

  \texttt{token,([\^{}\textbackslash{}r\textbackslash{}n,]*),separator}

  \texttt{separator,(?:\textbackslash{}r\textbackslash{}n|,),token}
\end{itemize}
\caption{Examples of text defined REDFA.}
\label{fig:textdef}
\end{figure}

\section{Whitespace handling}
Whitespaces in this context are everything that is not compared in the text. The whitespaces in line-oriented diff are new lines, in word diff the whitespaces are the actual whitespaces (spaces, tabs, newlines etc.). In the user-defined tokenization the whitespaces are parts of the text that are between the tokens. In the line-oriented diff, whitespaces do not need to be handled because all the whitespaces are always the same. In the user-specified tokenization whitespaces can be anything, thus they need to be handled. A simple example why whitespaces needs to be handled can be seen in \Cref{fig:simplewshandle}.

\begin{figure}
  \centering
  \begin{tikzpicture}[]
    \node[draw,align=left,label={file A}] at (0,1) (n1){a \\ b \\ c};
    \node[draw,align=left,label={file B}] at (0,-2) (n2){a \\ b \\ d};
    \node[draw,align=left,label={Generated patch}] at (3,0) (n3){t  1\\
    w  \textbackslash n \\
    t  2 \\
    w  \textbackslash n \\
    t- 3 \\
    t+ 4};
    \node[draw,align=left,label=below:{file A with different whitespace}] at (3,-3) (n4){a b c};
    \node[draw,align=left,label=85:{Result of patching}] at (6,-1.5) (n5){a b d};
    \draw[->] (n1) -- (n3) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (n2) -- (n3) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (n3) -- (n5) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (n4) -- (n5) node [midway, above, sloped] (TextNode1) {};
  \end{tikzpicture}
  \caption{Example of patch, not failing when whitespaces changed in original file.}
  \label{fig:simplewshandle}
\end{figure}

Whitespace changes between tokens which are not changed nor shown within the context are not found by the diff and the patch. The whitespace from the source file is going to be used as shown in \Cref{fig:wsnotfound} and the change is not detected. Only whitespace changes that are around the token (in the context) changes are found. When inserting a token, the whitespaces around the token being inserted, are inserted as well (as seen in \Cref{fig:wsinsert}). When deleting a token, the whitespaces around the token, that is being deleted are deleted too and the whitespace from the target file is inserted as shown in \Cref{fig:wsdeletion}. During tokenizing, this needs to be considered. It is advised not to leave crucial parts of the text as whitespace because the program is not able to determine which whitespaces are to be used or deleted.

When the whitespace change is shown in a context but it is not directly located next to a token change, it is considered the same way as whitespace changes on a different place where they are not a part of the patch file. Therefore, the change is not applied. An example is shown in \Cref{fig:wsdeletionnotdeleted}.


\begin{figure}
  \centering
  \begin{tikzpicture}
    \node[draw,label={Source file},align=left] at (0,3) (s1){\small t1 \\ ws1 \\ t2};
    \node[draw,label={Target file},align=left] at (0,0) (s2){\small t1 \\ ws2 \\ t2};
    \node[draw,label={-90:diff},align=left] at (3,0) (diff) {\small \emph{empty}};
    \node[draw,label={After patch},align=left] at (3,3) (s3){\small t1 \\ ws1 \\ t2};
    \draw[->] (s2) -- (diff) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (s1) -- (diff) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (s1) -- (s3) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (diff) -- (s3) node [midway, above, sloped] (TextNode1) {};
  \end{tikzpicture}
  \caption{The change of a whitespace between non changing tokens. The diff is not able to find such change and the patch cannot patch it.}
  \label{fig:wsnotfound}
\end{figure}

\begin{figure}
  \centering
  \begin{tikzpicture}
    \node[draw,label={Source file},align=left] at (0,3) (s1){\small t1 \\ t3};
    \node[draw,label={Target file},align=left] at (0,0) (s2){\small t1 \\ ws2 \\ t2  \\ t3};
    \node[draw,label={-90:diff},align=left] at (3,0) (diff) {\small t1 \\ +ws2 \\ +t2 \\ t3};
    \node[draw,label={After patch},align=left] at (3,3) (s3){\small t1 \\ ws2 \\ t2 \\ t3};
    \draw[->] (s2) -- (diff) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (s1) -- (diff) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (s1) -- (s3) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (diff) -- (s3) node [midway, above, sloped] (TextNode1) {};
  \end{tikzpicture}
  \begin{tikzpicture}
    \node[draw,label={Source file},align=left] at (0,3) (s1){\small t1 \\ ws1 \\ t3};
    \node[draw,label={Target file},align=left] at (0,0) (s2){\small t1 \\ ws2 \\ t2 \\ ws1 \\ t3};
    \node[draw,label={-90:diff},align=left] at (3,0) (diff) {\small t1 \\ +ws2 \\ +t2 \\ ws1 \\ t3};
    \node[draw,label={After patch},align=left] at (3,3) (s3){\small t1 \\ ws2 \\ t2 \\ ws1 \\ t3};
    \draw[->] (s2) -- (diff) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (s1) -- (diff) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (s1) -- (s3) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (diff) -- (s3) node [midway, above, sloped] (TextNode1) {};
  \end{tikzpicture}
  \caption{Insertion of token t2.}
  \label{fig:wsinsert}
\end{figure}

\begin{figure}
  \centering
  \begin{tikzpicture}
    \node[draw,label={Source file},align=left] at (0,3) (s1){\small t1 \\ ws1 \\ t2 \\ ws2 \\ t3};
    \node[draw,label={Target file},align=left] at (0,0) (s2){\small t1 \\ ws3 \\ t3};
    \node[draw,label={-90:diff},align=left] at (3,0) (diff) {\small t1 \\ -ws1 \\ -t2 \\ -ws2 \\ +ws3 \\ t3};
    \node[draw,label={After patch},align=left] at (3,3) (s3){\small t1 \\ ws3 \\ t3};
    \draw[->] (s2) -- (diff) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (s1) -- (diff) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (s1) -- (s3) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (diff) -- (s3) node [midway, above, sloped] (TextNode1) {};
  \end{tikzpicture}
  \caption{Deletion of token t2.}
  \label{fig:wsdeletion}
\end{figure}

\subsection{Resolving whitespace conflicts}
A conflict occurs when a whitespace in a patch file does not match the whitespace of the file that patch is applied to. When tokens in context match but whitespaces do not, the hunk can not be rejected as whitespaces are not significant. The whitespaces in patch files are used in the result. This example can be seen in \Cref{fig:wsdeletionpatch} --- for deletion case when the diff is running, the source file has ws3 between t1 and t2 and ws4 between t2 and t3. So that the information about the change from ws1 to ws2 in not lost, the whitespaces are saved into a separate whitespace file, for insertion case when the diff is running, the source file has ws4 between t1 and t3, ws4 was changed to ws1 before the patch was running, so ws1 is saved into a whitespace file not to lose the information about having it. If the whitespaces do not change, the information about their deletion is saved in the patch file, and thus there is no need for them to be stored again.

\begin{figure}
  \centering
  \begin{tikzpicture}
    \node[draw,label={Source file},align=left] at (0,5) (s1){\small t1 \\ ws1 \\ t2 \\ ws2 \\ t3};
    \node[draw,label={Patch file},align=left] at (0,0) (s2){\small t1 \\ -ws3 \\ -t2 \\ -ws4 \\ +ws5 \\ t3};
    \node[draw,label={Whitespaces},align=left] at (3,0) (ws) {\small t1 \\ ws1 \\ t2 \\ ws2 \\ t3};
    \node[draw,label={After patch},align=left] at (3,5) (s3){\small t1 \\ ws5 \\ t3};
    \draw[->] (s1) -- (ws) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (s2) -- (ws) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (s1) -- (s3) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (s2) -- (s3) node [midway, above, sloped] (TextNode1) {};
  \end{tikzpicture}
  \begin{tikzpicture}
    \node[draw,label={Source file},align=left] at (0,3) (s1){\small t1 \\ ws1 \\ t3};
    \node[draw,label={Patch file},align=left] at (0,0) (s2){\small t1 \\ -ws4 \\ +ws2 \\ +t2 \\ +ws3 \\ t3};
    \node[draw,label={Whitespaces},align=left] at (3,0) (ws) {\small t1 \\ ws1 \\  t3};
    \node[draw,label={After patch},align=left] at (3,3) (s3){\small t1 \\ ws2 \\ t2 \\ ws3 \\ t3};
    \draw[->] (s1) -- (ws) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (s2) -- (ws) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (s1) -- (s3) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (s2) -- (s3) node [midway, above, sloped] (TextNode1) {};
  \end{tikzpicture}
  \caption{Deletion and insertion with different whitespaces in the patch file.}
  \label{fig:wsdeletionpatch}
\end{figure}

\begin{figure}
  \centering
  \begin{tikzpicture}
    \node[draw,label={Source file},align=left] at (0,4) (s1){\small t1 \\ ws1 \\ t2 \\ ws2 \\ t3};
    \node[draw,label={Patch file},align=left] at (0,0) (s2){\small t1 \\ -ws1 \\ +ws3 \\ t2 \\ -ws2 \\  -t3 \\ +ws4 \\ +t4};
    \node[draw,label={After patch},align=left] at (3,2) (s3){\small t1 \\ ws1 \\ t2 \\ ws4 \\t4};
    \draw[->] (s1) -- (s3) node [midway, above, sloped] (TextNode1) {};
    \draw[->] (s2) -- (s3) node [midway, above, sloped] (TextNode1) {};
  \end{tikzpicture}
  \caption{Differing whitespace in the context but not directly next to the token change. ws1 is not changed into ws3 despite the fact that the patch knows about the change.}
  \label{fig:wsdeletionnotdeleted}
\end{figure}