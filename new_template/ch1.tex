\chapter{Algorithms for comparing text}
Utilities for comparing texts are used by programmers on a daily basis. Probably the biggest application of these utilities are version control systems. For the proper development of large projects, it is important to store all versions of previous projects as a form of communication among the programmers. Every additional version of a project is called a revision. Every revision, except for the first one, originates in the previous one. When the revision needs to be checked --- what has changed --- the differences between current revision and the one it originated from need to be shown.

The three main tools used in comparing text are diff, patch and merge. The diff serves as a data comparison tool which displays the differences between two files. The changes made in a standard format, so that both humans and machines can understand them, are displayed by the diff. An example of how colored side-by-side comparison of two files looks like can be seen in \Cref{fig:coloredsbs}. The patch utility takes a comparison output produced by the diff and applies the differences to a copy of the original file, producing a patched version. Diff3 is used when two people make changes to the same base file. It can produce a merged output that contains changes from both files and warnings when conflict appears.

\begin{figure}
    \centering
    \includegraphics[scale=0.5]{img/sbs_diff.jpg}
    \caption{The colored diff in a side-by-side format.}
    \label{fig:coloredsbs}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[scale=0.5]{img/colored_diff_laotzu.jpg}
    \caption{The colored diff in a context format.}
    \label{fig:coloredcontext}
\end{figure}

\section{Diff implementation}

The diff produces differences between two files. One way to achieve this is by computing the edit distance (as seen in \Cref{editdistance}) using the Wagner-Fischer algorithm (as seen in \Cref{wagnerfischer}) and using the output of the Wagner-Fischer to find a sequence of insertions, substitutions and deletions to get from one text to another (as seen in \Cref{backtrackmatrix}).

\subsection{Tokenization}
Before we describe algorithms, we need to explain what tokenization and tokens are. Tokenization is a process of demarcating sections of a string of input characters. Tokenizers are usually designed to use a regular grammar (although it usually can not be achieved). An output is a list of tokens. Unlike parsing, which is usually a context-free grammar, the output is an abstract syntax tree. The parsing results in obtaining more information about the input and it is, certainly, more complex. We are going to analyze the tokenization more thoroughly in the \Cref{chapter2}.

Parts which are compared in the text are tokens. In edit distance, each character is a single token. In diff, tokenization is done by splitting text with delimiters being newlines. Each line is a single token. Tokens are comparable --- it is possible to determine whether they are equal or not.

\subsection{Edit distance} \label{editdistance}

To be able to tell whether two strings are similar and by how much we need a metric that results in a number for two given strings. Many different types of edit distance exist. The most common being Levenshtein distance~\cite{wagner1974string}. It uses 3 edit operations: changing one character to another single character, deleting one character from a given string and inserting a single character into the given string. Another type of edit distance is called longest common subsequnce where only insertions and deletions are allowed, substitutions are not. The amount of such operations needed to change one string into another is called edit distance.

\iffalse
Let us consider these three editing operations:

\begin{itemize}
    \item Changing one character to another single character
    \item Deleting one character from a given string
    \item Inserting a single character into the given string
\end{itemize}

Using only these three editing operations we get the most common metric for edit distance. It is called Levenshtein distance~\cite{levenshtein1966binary}. The term Levenshtein distance is often used interchangeably with the term edit distance.


\begin{defn}[Notation]
    Let $A$ be a finite string (or sequence) of characters (or symbols).
    $A \langle i\rangle$  is the $i$-th character of string $A$.
    $A \langle i:j\rangle$ is the $i$-th through $j$-th characters (inclusive) of $A$.
    If $i > j$, $A\langle i:j\rangle = \Lambda, $ the null string.
    $|A|$ denotes the length (number of characters) of string $A$.~\cite{wagner1974string}
\end{defn}
So $A \langle i:j\rangle = A\langle i\rangle, A\langle i + 1\rangle \dots A\langle j\rangle)$
\begin{defn}[String edit]
    An edit operation is a pair $(a, b) \neq ( \Lambda, \Lambda)$ of strings of length less than or equal to 1 and is usually written as $a \rightarrow b$~\cite{wagner1974string}
\end{defn}
\begin{defn}[Editing operations]
    String $B$ results from the application of the operation $a \rightarrow b$ to string A, written $A \Rightarrow B$ via $a \rightarrow b$, if $A = \sigma a \tau $ and $B =  \sigma b \tau$ for some strings $\sigma$ and $\tau$. We call $a \rightarrow b$ a \emph{change or substitution} operation if $a \neq \Lambda$ and $b \neq \Lambda;$ a \emph{delete} operation if $b= \Lambda$; and an \emph{insert} operation if $a = \Lambda$~\cite{wagner1974string}.
\end{defn}

Let $S$ be a sequence $s_{l}, s_{2}, \dots , s_{m}$ of edit operations (or edit sequence for short). An $S$-derivation from $A$ to $B$ is a sequence of strings $A_{0}, A_{1}, \dots , A_{m}$ where $A = A_{0}, B = A_{m}$, and $A_{i-1} \Rightarrow A_{i}$ via $s_{i}$ for $1 \leq i \leq m.$ We say $S$ takes $A$ to $B$ if there is some $S$-derivation from $A$ to $B$~\cite{wagner1974string}.

Now let $\gamma$ be an arbitrary cost function which is assigned to each edit operation $a \rightarrow b$
a nonnegative real number  $\gamma(a \rightarrow b)$. Extend $\gamma$ to a sequence of edit operations $S = s_{l},s_{2}, \dots , s_{m}$ by letting $\gamma(S) = \sum_{i=1}^{m} \gamma (s_{i})$. (If $m = 0$, we define $\gamma (S) = 0$)~\cite{wagner1974string}.

\begin{defn}[Edit distance]
    Let $\gamma (A, B)$ from the string $A$ to the string $B$ be the minimum cost of all sequences of edit operations which transform $A$ into $B$. Formally, $\gamma (A, B) = min\{\gamma(S) | S$ is an edit sequence taking $A$ to $B$\}~\cite{wagner1974string}.
\end{defn}

\fi

\subsection{Wagner and Fischer algorithm} \label{wagnerfischer}
The Wagner-Fischer algorithm~\cite{wagner1974string} is an algorithm used for finding edit distance. There are two strings as an input (can be applied to any two lists of items that can be compared). The computing is based on the following observation. If we reserve a matrix to hold edit distances between all the prefixes of the first string and all the prefixes of the second one, then the values in the matrix can be computed by flood filling the matrix, and thus the distance between the two full strings can be determined as the last value computed. An example of such implementation can be observed in \Cref{alg:waf}.



\begin{defn}[Notation] Let $A$ and $B$ be arrays of tokens and $a$ and $b$ be the tokens. Define $A(i) = A\langle 1:i\rangle $, $B(j) = B\langle 1:j\rangle $, and $D(i,j) = \gamma(A(i),B(j)), 0 \leq i \leq |A|, 0 \leq j \leq |B|$.
\end{defn}
$\gamma ( a \rightarrow b )  $ is 0 if $a$ equals $b$ otherwise is 1

\begin{align*}
    D(i,j) = min\{ & D(i-1,j-1)+\gamma(A\langle i\rangle \rightarrow B\langle j\rangle ), \\
                   & D(i-1,j)+\gamma(A\langle i\rangle \rightarrow \Lambda),              \\
                   & D(i,j-1)+\gamma(\Lambda \rightarrow B\langle j\rangle )\}
\end{align*}
for all $i,j, 1 \leq i \leq |A|, 1 \leq j \leq |B|.$


$$D(0,0) = 0; D(i,0)=\sum_{r=1}^{i} \gamma (A\langle r \rangle \rightarrow \Lambda); D(0,j)=\sum_{r=1}^{j} \gamma (\Lambda \rightarrow B\langle r \rangle)$$



\subsection{Backtrack algorithm to find edit operations in matrix} \label{backtrackmatrix}
We can apply edit distance matrix of substrings filled by the Wagner Fischer algorithm to find edit operations. An example of such implementation can be seen in \Cref{alg:waf} as a backtrack function. The algorithm starts at the right bottom cell of the matrix (edit distance between the two full strings). It finds a path in which the last cell was taken to fill. The path is always nondecreasing and ambiguous. As an example solution for strings 'ac' and 'b' are deletion 'a' and substitution 'b' for 'c'. The second possible solution is substitution 'b' for 'a' and deletion 'c'. Both solutions are of length 2 and are correct.


\begin{algorithm}[p]
    \begin{algorithmic}[1] %first line number lol
        \Procedure{Wagner and Fischer}{$D$}
        \State $D[0,0] \gets 0$

        \For{$i\gets 1, |A|$}
        \State $D[i,0]\gets D[i-1,0]+\gamma (A\langle i\rangle \rightarrow \Lambda)$
        \EndFor

        \For{$j\gets 1, |B|$}
        \State $D[0,j]\gets D[0,j-1]+\gamma (\Lambda \rightarrow B\langle j\rangle)$
        \EndFor

        \For{$i\gets 1, |A|$}
        \For{$j\gets 1, |B|$}
        \State $m_{1} \gets D[i-1,j-1]+\gamma (A\langle i\rangle \rightarrow B\langle j\rangle)$
        \State $m_{2} \gets D[i-1,j]+\gamma (A\langle i\rangle \rightarrow \Lambda)$
        \State $m_{3} \gets D[i,j-1]+\gamma (\Lambda \rightarrow B\langle j\rangle)$
        \State $D[i,j] \gets min(m_{1},m_{2},m_{3})$
        \EndFor
        \EndFor
        \EndProcedure

        \algnewcommand\print{\textbf{print}}
        \algnewcommand\ands{\textbf{\ and\ }}

        \Procedure{Backtrack}{$D$}
        \State $i \gets|A|$
        \State $j \gets|B|$
        \While{$i\neq 0 \ands j\neq 0 $}
        \If{$D[i,j] = D[i-1,j]+\gamma (A\langle i\rangle \rightarrow \Lambda)$}
        \State $i \gets i-1$
        \State $\print("Addition: ",A\langle i\rangle)$
        \ElsIf{$D[i,j] = D[i,j-1]+\gamma (\Lambda \rightarrow B\langle j\rangle)$}
        \State $j \gets j-1$
        \State $\print("Deletion: ",B\langle j\rangle)$
        \Else
        \State $i \gets i-1$
        \State $j \gets j-1$
        \If{$A\langle i\rangle \neq B\langle j\rangle$}
        \State $\print("Substitution: ",A\langle i\rangle , B\langle j\rangle)$
        \EndIf
        \EndIf
        \EndWhile
        \EndProcedure

    \end{algorithmic}
    \caption{Wagner Fischer algorithm to fill a matrix with edit distances of substrings. The backtrack algorithm to determine edit operations.}
    \label{alg:waf}
\end{algorithm}


\subsection{Other algorithms used for diff purpose}

As edit operations are not uniquely determined, other diff algorithms can result in different outputs. This leads to that on various text formats different algorithms could provide better user readable outputs than the other ones. Sometimes it also could be beneficial not to find the smallest edit distance. This leads to better performance on some use cases. Myers' diff is one example of such algorithm~\cite{Myers86}. Its time complexity is $O((m+n)*d)$ where $m$ and $n$ are lengths and $d$ is number of edits. As we can see in scenarios where there are small amounts of edits in large files, this provides much better execution time. This is used in GIT version control system as default diff algorithm. Other example is Histogram algorithm which is derived from patience algorithm. It creates histogram of occurences for each element and tries to match positions recursively~\cite{histogram}.

\begin{figure}
    \begin{scriptsize}
  
      \begin{minipage}{.49\textwidth}
          \begin{lstlisting}[basicstyle=\footnotesize,keywordstyle=\ttfamily]{Name}
--- a/Testdata/t1.c
+++ b/Testdata/t2.c
@@ -1,9 +1,9 @@
-int add(int a, int b)
+int add(int a, int b, int c)
 {
-    return a + b;
+    return a + b + c;
 }
 
-int add(int a, int b, int c)
+int add(int a, int b)
 {
-    return a + b + c;
+    return a + b;
 }
      \end{lstlisting}
      \end{minipage}
      \begin{minipage}{.49\textwidth}
          \begin{lstlisting}[basicstyle=\footnotesize,keywordstyle=\ttfamily]{Name}
--- a/Testdata/t1.c
+++ b/Testdata/t2.c
@@ -1,9 +1,9 @@
-int add(int a, int b)
-{
-    return a + b;
-}
-
 int add(int a, int b, int c)
 {
     return a + b + c;
 }
+
+int add(int a, int b)
+{
+    return a + b;
+}
      \end{lstlisting}
      \end{minipage}
      \caption{Myers algorithm (left) and histogram algorithm (right). In this case histogram produce better arranged output, but has more edit operations. }
      \label{fig:myershistogram}
    \end{scriptsize}
  \end{figure}




\section{Merging and applying changes}

\subsection{Patch}

The output of the diff is not only for people but also for other programs as well. A patch is a program that takes an output of a diff and applies it. The utility that updates text files according to instructions is called a patch. The instructions are produced by the diff. This may seem to have no use. Comparing files and then applying changes to the first file, results in forming of the second file. The use of the patch is to be able to review the output of the diff, adjust or remove some of the changes and apply them afterwards.

The GNU Patch manual~\cite{diffutilsmanual} describes the patch algorithm as follows:

\begin{quotation}
    "The patch reads instructions and applies them to the file (as seen in \Cref{fig:patch}). As for context diffs, patch can detect when the line numbers mentioned in the patch are incorrect, and it attempts to find the correct place to apply each hunk of the patch. A hunk is a sequence of lines common to both files, interspersed with groups of differing lines. As a first guess, it takes the line number mentioned in the hunk, plus or minus any offset used in applying the previous hunk. If that is not the correct place, the patch makes a forward and a backward scan for a set of lines to match the context given in the hunk.

    At first, the patch looks for a place where all lines of the context match. If it cannot find such place, and it reads a context or a unified diff and the maximum fuzz factor is set to 1 or more, then the patch makes another scan, ignoring the first and the last line of the context. If that fails, and the maximum fuzz factor is set to 2 or more, it makes a scan again, ignoring the first two and the last two lines of context. It behaves similarly if the maximum fuzz factor is larger.

    If the patch cannot find a place to install a hunk of the patch, it writes the hunk out to a reject file. The line numbers on the hunks in the reject file may be different from those in the patch file: they show the approximate location where the patch thinks the failed hunks belong in the new file rather than in the old one.

    The patch usually produces correct results, even when it makes many guesses. However, the results are guaranteed only when the patch is applied to an exact copy of the file that the patch was generated from."
\end{quotation}


\begin{algorithm}
    \begin{algorithmic}[1]
        \State{$hunks \gets parseContext()$}
        \State{$file \gets readFile()$}
        \ForAll{$h \gets hunks$}
            \State{$p \gets position(h)$}
        \If{$h.match(file.atPosition(p))$}
            \State{$applyContext(h,file)$}
        \ElsIf{$h.match(neighborhood(file,p))$}
            \State{$applyContext(h,file)$}
        \Else
            \State{$saveRejectedHunk(h)$}
        \EndIf
        \EndFor
    \end{algorithmic}

    \caption{Patch pseudocode.}
    \label{alg:patch}
\end{algorithm}



\begin{figure}
    \centering
    \begin{tikzpicture}[font=\tt\small]
        \node[draw,align=left,label={File A}] at (0,3) (file){Line1 \\ Line2 \\ Line3 };
        \node[draw,align=left,label={Patch file for file A}] at (0,0) (patch){Delete Line1 \\ \\ Insert Line4 at the end};
        \node[draw,align=left,label={Result}] at (4,1.5)(result){Line2 \\ Line3 \\ Line4 };

        \draw[->] (file) -- (result) node [midway, above] (l1) {};
        \draw[->] (patch) -- (result) node [midway, above, sloped] (l2) {};

    \end{tikzpicture}
    \caption{Simplified patching.}
    \label{fig:patch}
\end{figure}

\subsection{Three-way merge}
A more interesting phenomenon than patching is merging. One possible way of merging is a three-way merge algorithm. Two files that are merged and their common ancestor (base) are considered. The result is a single file containing both sets of changes. Let us call chunk, fragments of text from all 3 files. Stable chunk is a chunk where all 3 fragments of text in files are the same. Unstable chunk is when fragments differ. Possibilities of unstable chunks are, changes in one of the merged files, falsely conflicting chunk --- changes in both merged files, but the change is the same and conflicting chunk --- all 3 fragments differ.


To sum it up, the result consists of:
\begin{itemize}
    \item The parts matching in all 3 files (stable chunk)
    \item The parts not matching the base in one of the merging files (unstable chunk)
    \item The parts not matching the base but matching each other (falsely confilicting unstable chunk)
    \item Placeholders for parts where all of them are different marked as conflict to be resolved (conflicting chunk)
\end{itemize}

An example with all types of chunks can be seen in \Cref{fig:merge}

Firstly it finds differences between the base and each merging file. It starts applying both changes on the base file from the beginning. If two differences which are not the same (unstable chunk) exist, one in A file and one in B, that should be applied on the same line (or adjacent to each other), algorithm can not decide which change to apply or how to merge them together. It marks the place where differences should be applied as conflict and it is left to the user to be resolved. All other differences, that do not interfere with each other or are of the same change on the same line can be applied on the base file.




\iffalse
One possible way of merging is using a three-way merge algorithm. Two files that merge and their common ancestor (base) are considered. All three files are compared and the result consists of:


\begin{defn}[Notation]
    We assume given some set of \texttt{atoms} $A$. (In practice, these might be lines of text, as in GNU diff3, or they could be words, characters, tokens etc.). We write $A \text{*}$ for the set of lists with elements drawn from $A$ and use variables J, K, L, O, A, B and C to stand for elements of $A \text{*}$. If $L$ is a list and $k \in {1,\dots, |L|}$, then $L[k]$ denotes the $k$-th element of $L$. A \texttt{span} in a list $L$ is a pair of indices $[i..j]$ with $1 \leq i, j \leq |L|$~\cite{diff3}.
\end{defn}

\begin{defn}[Configuration]
    A configuration is a triple $(A, O, B) \in A\text{*} \times A\text{*} \times A\text{*}$. We usually write configurations in the more suggestive notation $(A \leftarrow O \rightarrow B)$ to emphasize that $O$ is the archive from which $A$ and $B$ have been derived~\cite{diff3}.
\end{defn}

The first step of \texttt{diff3} is to call a two-way comparison subroutine on $(O, A)$ and $(O, B)$ to compute a non-crossing matching $M_A$ between the indices of $O$ and $A$--that is, a boolean function on pairs of indices from $O$ and $A$ such that if $M_A[i, j]$ = true then (a) $O[i] = A[j]$, (b) $M_A[i', j] = false$ and $M_A[i, j'] = false$ whenever $i' \neq i$ and $j' \neq j$, and (c) $M_A[i ', j'] = false$ whenever either $i' < i$ and $j' > j$ or $i' > i$ and $j' < j$--and a non-crossing matching $M_B$ between the indices of $O$ and $B$. We treat this algorithm as a black box, simply assuming (a) that it is deterministic, and (b) that it always yields maximum matchings~\cite{diff3}.

\begin{defn}[Chunk]
    A chunk (from $A$, $O$, and $B$) is a triple $H = ([a_i..a_j ],\newline [o_i..o_j ], [b_i..b_j ])$ of a span in $A$, a span in $O$, and a span in $B$ so that at least one of the three is non-empty. The size of a chunk is the sum of the lengths of all three spans. Write $A[H]$ for $A[a_i..a_j] \in A\text{*}$, and similarly $O[H] = O[o_i..o_j ]$ and $B[H] = B[b_i..b_j ]$~\cite{diff3}.
\end{defn}

\begin{defn}[Stable chunk]
    A stable chunk is a chunk in which all three spans have the same length and corresponding indices are matched in all three --- i.e., a hunk $([a..a+k-1], [o..o+k-1], [b..b+k-1])$ for some $k > 0$, with $M_A[o+i, a+i] = M_B[o+i, b+i] = true$ for each $0 \leq i < k$. That is, a table chunk corresponds to a span in $O$ that is matched in both $M_A$ and $M_B$~\cite{diff3}.
\end{defn}

\begin{defn}[Unstable chunk]
    An unstable chunk is one that is not stable. An unstable chunk H is classified as follows:~\cite{diff3}\newline
    \begin{tabbing}
        \hspace{1em} \= $H$ is changed in $A$ \hspace{2em} \= if $O[H] = B[H] \neq A[H]$\\
        \> $H$ is changed in $B$ \> if $O[H] = A[H] \neq B[H]$\\
        \> $H$ is falsely conflicting \> if $O[H] \neq A[H] = B[H]$ \\
        \> $H$ is conflicting \> if $O[H] \neq A[H] \neq B[H]$
    \end{tabbing}
\end{defn}

An example with all types of chunks can be seen in \Cref{fig:merge}


\begin{algorithm}
    \begin{enumerate}
        \item Initialize $l_O = l_A = l_B = 0$. Find matching $M_A$ and $M_B$.
        \item Find the least positive integer $i$ so that either $M_A[l_O +i, l_A +i] = false$ or $M_B[l_O + i, l_B + i] = false$. If $i$ does not exist, then skip to step 3 to output a final stable chunk.
              \begin{enumerate}
                  \item If $i = 1$, then find the least integer $o > l_O$ such that there exist indices $a, b$ with $M_A[o, a] = M_B[o, b] = true$. If $o$ does not exist, then skip to step 3 to output a final unstable chunk. Otherwise, output the (unstable) chunk.
                        $C = ([l_A + 1 .. a - 1], [l_O + 1 .. o - 1], [l_B + 1 .. b - 1])$
                  \item  If $i > 1$, output the (stable) chunk
                        $C = ([l_A + 1 .. l_A + i - 1], [l_O + 1 .. l_O + i - 1], [l_B + 1 .. l_B + i - 1])$,
                        Set $l_O = l_O + i - 1, l_A = l_A + i - 1, l_B = l_B + i - 1,$ and repeat step 2.
              \end{enumerate}
        \item If $(l_O < |O| or l_A < |A| or l_B < |B|)$, output a final chunk
              $C = ([l_A + 1 .. |A|], [l_O + 1 .. |O|], [l_B + 1 .. |B|])$.
    \end{enumerate}
    \caption{Three-way merge algorithm~\cite{diff3}.}
    \label{fig:threewaymerge}
\end{algorithm}



\fi

\begin{figure}
    \centering
    \begin{tikzpicture}
        \node[draw,align=left,label={Base file O}] at (0,2) (base){A4 \\  B5 \\ C3 \\ D2 \\ E6 };
        \node[draw,align=left,label={Merging file A}] at (4,4) (fileb){A4 \\  \underline{B6} \\ C3 \\ \underline{D3} \\ \underline{E7}};
        \node[draw,align=left,label={Merging file B}] at (4,0)(filec){A4 \\  B5 \\ \underline{C4} \\ \underline{D4} \\ \underline{E7}};
        \node[draw,align=left,label={Result}] at (8,2)(result){A4 \\  \textbf{B6} \\ \textbf{C4} \\ \textbf{Conflict} \\ \textbf{E7}};

        \draw[->] (base) -- (fileb) node [midway, above] (l1) {};
        \draw[->] (base) -- (filec) node [midway, above, sloped] (l2) {};
        \draw[->] (fileb) -- (result) node [midway, above, sloped] (l3) {};
        \draw[->] (filec) -- (result) node [midway, above, sloped] (l4) {};

    \end{tikzpicture}
    \caption{The three-way merge with the base file O and two files A and B. The result is composed of: A4, a stable chunk. B6 changed in A. C4 changed in B. Next, there is a truly conflicting chunk. E7 is a falsely conflicting chunk.}
    \label{fig:merge}
\end{figure}






