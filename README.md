# Token aware diff

## Diff

### Command line arguments
    -C, --context *number*
Optional. Prints *number* of context lines in hunks

    -a, --automaton *definition of automaton*

Sets the automaton definition

    -f, --file-automaton *path to file with definitions*

Using file with definitions

    -d, --debug

For tokenization debug. Do not compare files, only show how file1 is tokenized.

### Examples
    tdiff *file1* *file2* -a 1,(\S*),2;2,\s*,1

    tdiff *file1* *file2* -f *definition_file* -C 3

    tdiff *file1* -f *definition_file* -d

Definition file example for csv with header:

    header,.*\r\n,line
    line,([^\r\n;]*),separator
    separator,(?:\r\n|;),line

## Patch

### Command line arguments
    -i, --ignorews
Ignore  whitespaces  during  patching. Only the tokens are inserted and deleted.

    -o, --output *file*
Create new file instead of applying patches to file specified in patch file

### Examples
    tpatch patchfile -o output

Where patchfile is in tdiff output format

## Diff3

### Command line arguments


    -a, --automaton *definition of automaton*

Sets the automaton definition

    -f, --file-automaton *path to file with definitions*

Using file with definitions

### Examples
    tdiff3 *file1* *basefile* *file2* -a 1,(\S*),2;2,\s*,1

    tdiff3 *file1* *basefile* *file2* -f *definition_file*

## Git integration

    git config --global diff.tool tdiff
    git config --global difftool.tdiff.cmd "/path/to/tdiff \$LOCAL \$REMOTE -f /path/to/file/with/definitions"
    git config --global difftool.tdiff.trustExitCode false

    git config --global merge.tool tdiff3
    git config --global mergetool.tdiff3.cmd "/path/to/tdiff3 \$LOCAL \$BASE \$REMOTE -f /path/to/file/with/definitions"
    git config --global mergetool.tdiff3.trustExitCode false

## Running tests
[Bats](https://github.com/sstephenson/bats) is needed to run test. After compiliing with make just run

    cd Tests
    bats test.bats